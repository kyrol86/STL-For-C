#ifndef QUEUE_TYPE
#error There must be a provided type for QUEUE_TYPE and it must be defined prior to using this code!
#define QUEUE_TYPE int32_t // To make VSCode happy
#endif

#ifndef INDEX_TYPE
#define INDEX_TYPE uint64_t
#endif

#ifndef ALLOW_SHRINKING_QUEUE
#define ALLOW_SHRINKING_QUEUE 0
#endif

#ifndef STL_FOR_C_IMPLEMENTATION_ONLY
#define STL_FOR_C_IMPLEMENTATION_ONLY
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define MAKE_QUEUE_NAME(x) queue_##x
#define QUEUE_NAME(x) MAKE_QUEUE_NAME(x)
#define QUEUE QUEUE_NAME(QUEUE_TYPE)

#ifndef MINIMUM_QUEUE_SIZE
#define MINIMUM_QUEUE_SIZE 32
#endif

typedef struct QUEUE
{
    QUEUE_TYPE *items;
    INDEX_TYPE tail;  // For Enqueue
    INDEX_TYPE head; // For Dequeue
    INDEX_TYPE size; // Items enqueued
    INDEX_TYPE capacity; // Memory Allocated for Items
} QUEUE;

#define MAKE_QUEUE_NEW_NAME(x) x##_new()
#define GEN_NEW_NAME(x) MAKE_QUEUE_NEW_NAME(x)

QUEUE *GEN_NEW_NAME(QUEUE)
{
    QUEUE *newQueue = (QUEUE *)malloc(sizeof(QUEUE));
    if (newQueue == NULL)
        return NULL;
    newQueue->items = (QUEUE_TYPE *)malloc(sizeof(QUEUE_TYPE) * MINIMUM_QUEUE_SIZE);
    if (newQueue->items == NULL)
    {
        free(newQueue);
        return NULL;
    }
    newQueue->head = 0;
    newQueue->tail = 0;
    newQueue->size = 0;
    newQueue->capacity = MINIMUM_QUEUE_SIZE;
    return newQueue;
}

#undef GEN_NEW_NAME
#undef MAKE_QUEUE_NEW_NAME

#define MAKE_QUEUE_NEW_CAPACITY_NAME(x, y) x##_new_withcapacity(y initialCapacity)
#define GEN_NEW_CAPACITY_NAME(x, y) MAKE_QUEUE_NEW_CAPACITY_NAME(x, y)

QUEUE *GEN_NEW_CAPACITY_NAME(QUEUE, INDEX_TYPE)
{
    QUEUE *newQueue = (QUEUE *)malloc(sizeof(QUEUE));
    if (newQueue == NULL)
        return NULL;
    if (initialCapacity < MINIMUM_QUEUE_SIZE)
        initialCapacity = MINIMUM_QUEUE_SIZE;
    newQueue->items = (QUEUE_TYPE *)malloc(sizeof(QUEUE_TYPE) * initialCapacity);
    if (newQueue->items == NULL)
    {
        free(newQueue);
        return NULL;
    }
    newQueue->head = 0;
    newQueue->tail = 0;
    newQueue->size = 0;
    newQueue->capacity = initialCapacity;
    return newQueue;
}

#undef GEN_NEW_CAPACITY_NAME
#undef MAKE_QUEUE_NEW_CAPACITY_NAME

#define MAKE_QUEUE_DESTROY_NAME(x) x##_destroy(QUEUE *this)
#define GEN_DESTROY_NAME(x) MAKE_QUEUE_DESTROY_NAME(x)

bool GEN_DESTROY_NAME(QUEUE)
{
    if (this == NULL)
        return false;
    if (this->items != NULL)
        free(this->items);
    free(this);
    return false;
}

#undef GEN_DESTROY_NAME
#undef MAKE_QUEUE_DESTROY_NAME

#define MAKE_QUEUE_CLEAR_NAME(x) x##_clear(QUEUE *this)
#define GEN_CLEAR_NAME(x) MAKE_QUEUE_CLEAR_NAME(x)

bool GEN_CLEAR_NAME(QUEUE)
{
    if (this == NULL)
        return true;

    if (this->size > MINIMUM_QUEUE_SIZE)
    {
        QUEUE_TYPE* ptr = (QUEUE_TYPE*) realloc(this->items, MINIMUM_QUEUE_SIZE);
        if (ptr == NULL)
            return true;
        this->items = ptr;
        this->capacity = MINIMUM_QUEUE_SIZE;
    }
    this->head = 0;
    this->tail = 0;
    this->size = 0;
    return false;
}

#undef GEN_CLEAR_NAME
#undef MAKE_QUEUE_CLEAR_NAME

#define MAKE_QUEUE_GETCOUNT_NAME(x) x##_getcount(QUEUE *this, INDEX_TYPE *outCount)
#define GEN_GETCOUNT_NAME(x) MAKE_QUEUE_GETCOUNT_NAME(x)

bool GEN_GETCOUNT_NAME(QUEUE)
{
    if (this == NULL || outCount == NULL)
        return true;
    *outCount = this->size;
    return false;
}

#undef GEN_GETCOUNT_NAME
#undef MAKE_QUEUE_GETCOUNT_NAME

#define MAKE_QUEUE_REALLOCATE_NAME(x) x##_reallocate(QUEUE *this, INDEX_TYPE newSize)
#define GEN_REALLOCATE_NAME(x) MAKE_QUEUE_REALLOCATE_NAME(x)

bool GEN_REALLOCATE_NAME(QUEUE)
{
    if (this == NULL)
        return true;
    if (newSize <= this->size)
        return true;
    QUEUE_TYPE* ptr = (QUEUE_TYPE*)malloc(sizeof(QUEUE_TYPE) * newSize);
    if (ptr == NULL)
        return true;
    if (this->tail > this->head)
    {
        INDEX_TYPE tailToTopSize = this->size - this->tail + 1;
        memcpy(&ptr[tailToTopSize], this->items, this->head * sizeof(QUEUE_TYPE));
        memcpy(ptr, &this->items[this->tail], tailToTopSize * sizeof(QUEUE_TYPE));
        this->head = tailToTopSize + this->head;
    }
    else
    {
        memcpy(ptr, &this->items[this->tail], (this->head - this->tail) * sizeof(QUEUE_TYPE));
        this->head = this->head - this->tail;
    }
    free(this->items);
    this->items = ptr;
    this->tail = 0;
    this->capacity = newSize;
    return false;
}

#undef GEN_REALLOCATE_NAME
#undef MAKE_QUEUE_REALLOCATE_NAME

#define MAKE_QUEUE_ENQUEUE_NAME(x, y, z) x##_enqueue(x *this, y item, z *outEnqueuedCount)
#define GEN_ENQUEUE_NAME(x, y, z) MAKE_QUEUE_ENQUEUE_NAME(x, y, z)

bool GEN_ENQUEUE_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE)
{
    #define MAKE_QUEUE_REALLOCATE_NAME(x) x##_reallocate
    #define GEN_REALLOCATE_NAME(x) MAKE_QUEUE_REALLOCATE_NAME(x)

    if (this == NULL)
        return true;
    if (this->capacity <= this->size + 1)
        if (GEN_REALLOCATE_NAME(QUEUE)(this, this->capacity * 2))
            return true;
    this->tail = (this->tail - 1) % this->capacity;
    this->items[this->tail] = item;
    this->size++;
    if (outEnqueuedCount != NULL)
        *outEnqueuedCount = 1;
    
    return false;

    #undef GEN_REALLOCATE_NAME
    #undef MAKE_QUEUE_REALLOCATE_NAME
}

#undef GEN_ENQUEUE_NAME
#undef MAKE_QUEUE_ENQUEUE_NAME

#define MAKE_QUEUE_ENQUEUE_ITEMS_NAME(x, y, z) x##_enqueue_items(x *this, y *items, z items_index, z items_count, z *outEnqueuedCount)
#define GEN_ENQUEUE_ITEMS_NAME(x, y, z) MAKE_QUEUE_ENQUEUE_ITEMS_NAME(x, y, z)

bool GEN_ENQUEUE_ITEMS_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE)
{
    #define MAKE_QUEUE_REALLOCATE_NAME(x) x##_reallocate
    #define GEN_REALLOCATE_NAME(x) MAKE_QUEUE_REALLOCATE_NAME(x)
    
    if (this == NULL || this->items == NULL)
        return true;
    if (items_count <= 0)
        return false;
    // Reallocate items if reaching capacity
    INDEX_TYPE newSize = this->size + items_count;
    if (newSize > this->capacity)
    {
        INDEX_TYPE newCapacity = this->capacity * 2;
        while (newCapacity < newSize)
        {
            newCapacity *= 2;
        }
        if (GEN_REALLOCATE_NAME(QUEUE)(this, newCapacity))
            return true;
    }
    
    // Enqueue the data!
    INDEX_TYPE index = items_index;
    for (INDEX_TYPE remainingCount = items_count; remainingCount > 0; remainingCount--)
    {
        this->tail = (this->tail - 1) % this->capacity;
        this->items[this->tail] = items[index++];
    }
    this->size = newSize;
    if (outEnqueuedCount != NULL)
        *outEnqueuedCount = items_count;
    
    return false;
    #undef GEN_REALLOCATE_NAME
    #undef MAKE_QUEUE_REALLOCATE_NAME
}

#undef GEN_ENQUEUE_ITEMS_NAME
#undef MAKE_QUEUE_ENQUEUE_ITEMS_NAME

#define MAKE_QUEUE_DEQUEUE_NAME(x, y, z) x##_dequeue(x *this, y *outItem, z* outDequeueCount)
#define GEN_DEQUEUE_NAME(x, y, z) MAKE_QUEUE_DEQUEUE_NAME(x, y, z)
bool GEN_DEQUEUE_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE)
{
    #define MAKE_QUEUE_REALLOCATE_NAME(x) x##_reallocate
    #define GEN_REALLOCATE_NAME(x) MAKE_QUEUE_REALLOCATE_NAME(x)

    if (this == NULL)
        return true;
    if (this->size <= 0)
    {
        if (outDequeueCount != NULL)
            *outDequeueCount = 0;
        return false;
    }
#if ALLOW_SHRINKING_QUEUE == 1
    if (this->capacity > (this->size - 1) * 4 && this->capacity / 2 >= MINIMUM_QUEUE_SIZE)
        if (GEN_REALLOCATE_NAME(QUEUE)(this, this->capacity / 2))
            return true;
#endif
    this->head = (this->head - 1) % this->capacity;
    *outItem = this->items[this->head];    
    this->size--;
    if (outDequeueCount != NULL)
        *outDequeueCount = 1;
    
    return false;

    #undef GEN_REALLOCATE_NAME
    #undef MAKE_QUEUE_REALLOCATE_NAME
}
#undef GEN_DEQUEUE_NAME
#undef MAKE_QUEUE_DEQUEUE_NAME

#define MAKE_QUEUE_DEQUEUE_ITEMS_NAME(x, y, z) x##_dequeue_items(x *this, z count, y *outItems, z *outDequeueCount)
#define GEN_DEQUEUE_ITEMS_NAME(x, y, z) MAKE_QUEUE_DEQUEUE_ITEMS_NAME(x, y, z)

bool GEN_DEQUEUE_ITEMS_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE)
{
    if (this == NULL || this->items == NULL)
        return true;
    if (count <= 0)
    {
        if (outDequeueCount != NULL)
            *outDequeueCount = 0;
        return false;
    }
    INDEX_TYPE maxCount = count > this->size ? this->size : count;
    for (INDEX_TYPE i = 0; i < maxCount; ++i)
    {
        this->head = (this->head - 1) % this->capacity;
        outItems[i] = this->items[this->head];
    }
    this->size -= maxCount;
    if (outDequeueCount != NULL)
      *outDequeueCount = maxCount;
    #if ALLOW_SHRINKING_QUEUE == 1
    if (this->capacity > (this->size - 1) * 4 && this->capacity / 2 >= MINIMUM_QUEUE_SIZE)
        if (GEN_REALLOCATE_NAME(QUEUE)(this, this->capacity / 2))
            return true;
#endif
    
    return false;
}

#undef GEN_DEQUEUE_ITEMS_NAME
#undef MAKE_QUEUE_DEQUEUE_ITEMS_NAME

#define MAKE_QUEUE_PEEK_NAME(x, y, z) x##_peek(x *this, y *outItem, z* outPeekCount)
#define GEN_PEEK_NAME(x, y, z) MAKE_QUEUE_PEEK_NAME(x, y, z)
bool GEN_PEEK_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE)
{
    if (this == NULL)
      return true;
    if (this->size <= 0)
    {
        if (outPeekCount != NULL)
            *outPeekCount = 0;
        return false;
    }
    
    INDEX_TYPE head = (this->head - 1) % this->capacity;
    *outItem = this->items[head];
    if (outPeekCount != NULL)
        *outPeekCount = 1;
    return false;    
}
#undef GEN_PEEK_NAME
#undef MAKE_QUEUE_PEEK_NAME

#define MAKE_QUEUE_PEEK_ITEMS_NAME(x, y, z) x##_peek_items(x *this, z count, y *outItems, z *outPeekCount)
#define GEN_PEEK_ITEMS_NAME(x, y, z) MAKE_QUEUE_PEEK_ITEMS_NAME(x, y, z)

bool GEN_PEEK_ITEMS_NAME(QUEUE, QUEUE_TYPE, INDEX_TYPE)
{
    if (this == NULL || this->items == NULL)
        return true;
    if (count <= 0)
    {
        if (outPeekCount != NULL)
            *outPeekCount = 0;
        return false;
    }
    INDEX_TYPE maxCount = count > this->size ? this->size : count;
    INDEX_TYPE head = this->head;
    for (INDEX_TYPE i = 0; i < maxCount; ++i)
    {
        head = (head - 1) % this->capacity;
        outItems[i] = this->items[head];
    }
    if (outPeekCount != NULL)
        *outPeekCount = maxCount;
    return false;
}

#undef GEN_PEEK_ITEMS_NAME
#undef MAKE_QUEUE_PEEK_ITEMS_NAME

#undef STL_FOR_C_IMPLEMENTATION_ONLY
#undef INDEX_TYPE
#undef QUEUE_TYPE
#undef QUEUE
#undef QUEUE_NAME
#undef MAKE_QUEUE_NAME
#undef MINIMUM_QUEUE_SIZE
