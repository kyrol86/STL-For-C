#include <stdlib.h>
#include <string.h>
#include <stdatomic.h>
#include <stdio.h>
#ifndef STL_FOR_C_IMPLEMENTATION_ONLY
    #define STL_FOR_C_IMPLEMENTATION_ONLY
#endif
#include "include/concurrent_stack.h"

#define MAKE_NODE_NAME(x) concurrent_stack_node_ ## x
#define NODE_NAME(x) MAKE_NODE_NAME(x)
#define NODE NODE_NAME(CONCURRENT_STACK_TYPE)

#define MAKE_CONCURRENT_STACK_NAME(x) concurrent_stack_ ## x
#define CONCURRENT_STACK_NAME(x) MAKE_CONCURRENT_STACK_NAME(x)
#define CONCURRENT_STACK CONCURRENT_STACK_NAME(CONCURRENT_STACK_TYPE)

#define MAKE_CONCURRENT_STACK_NEW_NAME(x) x ## _new()
#define GEN_NEW_NAME(x) MAKE_CONCURRENT_STACK_NEW_NAME(x)
CONCURRENT_STACK* GEN_NEW_NAME(CONCURRENT_STACK)
{
    return (CONCURRENT_STACK*)calloc(sizeof(CONCURRENT_STACK), 1);
}

#define MAKE_CONCURRENT_STACK_NEW_NAME2(x, y) x ## _new2(y* data, INDEX_TYPE length)
#define GEN_NEW_NAME2(x, y) MAKE_CONCURRENT_STACK_NEW_NAME2(x, y)
CONCURRENT_STACK* GEN_NEW_NAME2(CONCURRENT_STACK, CONCURRENT_STACK_TYPE)
{
    CONCURRENT_STACK* stack = (CONCURRENT_STACK*)calloc(sizeof(CONCURRENT_STACK), 1);
    NODE* lastNode = NULL;
    for (INDEX_TYPE i = 0; i < length; ++i)
    {
        NODE* newNode = (NODE*)calloc(sizeof(NODE), 1);
        newNode->m_value = data[i];
        newNode->m_next = lastNode;
        lastNode = newNode;
    }

    stack->m_head = lastNode;
    return stack;
}

#define MAKE_CONCURRENT_STACK_DESTROY_NAME(x) x ## _destroy(x* this)
#define GEN_DESTROY_NAME(x) MAKE_CONCURRENT_STACK_DESTROY_NAME(x)
bool GEN_DESTROY_NAME(CONCURRENT_STACK)
{
    NODE* ptr = atomic_exchange(&this->m_head, NULL);
    for (; ptr != NULL;)
    {
        NODE* next = (NODE*)ptr->m_next;
        free(ptr);
        ptr = next;
    }
    free(this);
    return false;
}

#define MAKE_CONCURRENT_STACK_ISEMPTY_NAME(x) x ## _isempty(x* this)
#define GEN_ISEMPTY_NAME(x) MAKE_CONCURRENT_STACK_ISEMPTY_NAME(x)
bool GEN_ISEMPTY_NAME(CONCURRENT_STACK)
{
    if (this == NULL) return true;
    NODE* head = atomic_load(&this->m_head);
    return head == NULL;
}

#define MAKE_CONCURRENT_STACK_COUNT_NAME(x) x ## _count(x* this)
#define GEN_COUNT_NAME(x) MAKE_CONCURRENT_STACK_COUNT_NAME(x)
INDEX_TYPE GEN_COUNT_NAME(CONCURRENT_STACK)
{
    if (this == NULL) return 0;
    INDEX_TYPE count = 0;
    for (NODE* current = this->m_head; current != NULL; current = current->m_next)
    {
        ++count;
    }
    return count;
}

#define MAKE_CONCURRENT_STACK_CLEAR_NAME(x) x ## _clear(x* this)
#define GEN_CLEAR_NAME(x) MAKE_CONCURRENT_STACK_CLEAR_NAME(x)
bool GEN_CLEAR_NAME(CONCURRENT_STACK)
{
    if (this == NULL) return true;
    NODE* original = NULL;
    original = atomic_exchange(&this->m_head, NULL);
    if (original == NULL)
        return false;
    for (NODE* current = original; current != NULL;)
    {
        NODE* next = current->m_next;
        free(current);
        current = next;
    }
    return false;
}

#define MAKE_CONCURRENT_STACK_PUSH_NAME(x, y) x ## _push(x* this, y item)
#define GEN_PUSH_NAME(x, y) MAKE_CONCURRENT_STACK_PUSH_NAME(x, y)
bool GEN_PUSH_NAME(CONCURRENT_STACK, CONCURRENT_STACK_TYPE)
{
    if (this == NULL) return true;
    NODE* newNode = (NODE*)calloc(sizeof(NODE), 1);
    if (newNode == NULL) return true;
    newNode->m_value = item;
    newNode->m_next = this->m_head;
    while (!atomic_compare_exchange_strong(&this->m_head, (NODE**) &newNode->m_next, newNode))
    {
        atomic_signal_fence(__ATOMIC_ACQ_REL);
    }
    return false;
}

#define MAKE_CONCURRENT_STACK_PUSHRANGE_NAME(x, y) x ## _pushrange(x* this, y* items, INDEX_TYPE length)
#define GEN_PUSHRANGE_NAME(x, y) MAKE_CONCURRENT_STACK_PUSHRANGE_NAME(x, y)
bool GEN_PUSHRANGE_NAME(CONCURRENT_STACK, CONCURRENT_STACK_TYPE)
{
    if (this == NULL || length <= 0) return true;
    NODE* head = (NODE*)calloc(sizeof(NODE), 1);
    if (head == NULL) return true;
    NODE* curr = head;
    head->m_value = items[length - 1];
    for (INDEX_TYPE i = length - 2; i > -1; --i)
    {
        curr->m_next = calloc(sizeof(NODE), 1);
        if (curr->m_next == NULL)
        {
            for (NODE* cleanup = head; cleanup != NULL;)
            {
                NODE* ptr = cleanup->m_next;
                free(cleanup);
                cleanup = ptr;
            }
            return true;
        }
        curr = curr->m_next;
        curr->m_value = items[i];
    }
    curr->m_next = this->m_head;
    while (!atomic_compare_exchange_strong(&this->m_head, (NODE**) &curr->m_next, head))
    {
        atomic_signal_fence(__ATOMIC_ACQ_REL);
    }
    return false;
}

#define MAKE_CONCURRENT_STACK_TRYPEEK_NAME(x, y) x ## _trypeek(x* this, y* result)
#define GEN_TRYPEEK_NAME(x, y) MAKE_CONCURRENT_STACK_TRYPEEK_NAME(x, y)
bool GEN_TRYPEEK_NAME(CONCURRENT_STACK, CONCURRENT_STACK_TYPE)
{
    if (this == NULL) return true;
    NODE* node = (NODE*) atomic_load(&this->m_head);
    *result = node->m_value;
    return false;
}

#define MAKE_CONCURRENT_STACK_TRYPOP_NAME(x, y) x ## _trypop(x* this, y* result)
#define GEN_TRYPOP_NAME(x, y) MAKE_CONCURRENT_STACK_TRYPOP_NAME(x, y)
bool GEN_TRYPOP_NAME(CONCURRENT_STACK, CONCURRENT_STACK_TYPE)
{
    if (this == NULL) return true;
    NODE* head = this->m_head;
    if (head == NULL)
        return true;
    while (!atomic_compare_exchange_strong(&this->m_head, (NODE**)&head, (NODE*)head->m_next))
    {
        if (head == NULL)
            return true;
        atomic_signal_fence(__ATOMIC_ACQ_REL);
    }
    *result = head->m_value;
    //free(head);
    return false;
}

#define MAKE_CONCURRENT_STACK_TRYPOPRANGE_NAME(x, y) x ## _trypoprange(x* this, y* result, INDEX_TYPE* result_length, INDEX_TYPE count)
#define GEN_TRYPOPRANGE_NAME(x, y) MAKE_CONCURRENT_STACK_TRYPOPRANGE_NAME(x, y)
bool GEN_TRYPOPRANGE_NAME(CONCURRENT_STACK, CONCURRENT_STACK_TYPE)
{
    if (this == NULL || count <= 0) return true;
    NODE* head;
    INDEX_TYPE i = 0;
    for (; i < count; ++i)
    {
        head = atomic_load(&this->m_head);
        if (head == NULL)
        {
            *result_length = i;
            return false;
        }
        while (!atomic_compare_exchange_strong(&this->m_head, &head, head->m_next))
        {
            if (head == NULL)
            {
                *result_length = i;
                return false;
            }
            atomic_signal_fence(__ATOMIC_ACQ_REL);
        }
        if (head == NULL)
        {
            *result_length = i;
            return false;
        }
        result[i] = head->m_value;
        if (head->m_next == NULL)
        {
            //free((void*)head);
            break;
        }
        //free((void*)head);
    }
    *result_length = i;
    return false;
}

#undef GEN_TRYPOPRANGE_NAME
#undef MAKE_CONCURRENT_STACK_TRYPOPRANGE_NAME
#undef GEN_TRYPOP_NAME
#undef MAKE_CONCURRENT_STACK_TRYPOP_NAME
#undef GEN_TRYPEEK_NAME
#undef MAKE_CONCURRENT_STACK_TRYPEEK_NAME
#undef GEN_PUSHRANGE_NAME
#undef MAKE_CONCURRENT_STACK_PUSHRANGE_NAME
#undef GEN_PUSH_NAME
#undef MAKE_CONCURRENT_STACK_PUSH_NAME
#undef GEN_CLEAR_NAME
#undef MAKE_CONCURRENT_STACK_CLEAR_NAME
#undef GEN_COUNT_NAME
#undef MAKE_CONCURRENT_STACK_COUNT_NAME
#undef GEN_ISEMPTY_NAME
#undef MAKE_CONCURRENT_STACK_ISEMPTY_NAME
#undef GEN_DESTROY_NAME
#undef MAKE_CONCURRENT_STACK_DESTROY_NAME
#undef GEN_NEW_NAME2
#undef MAKE_CONCURRENT_STACK_NEW_NAME2
#undef GEN_NEW_NAME
#undef MAKE_CONCURRENT_STACK_NEW_NAME

#undef MAKE_NODE_NAME
#undef NODE_NAME
#undef NODE
#undef MAKE_CONCURRENT_STACK_NAME
#undef CONCURRENT_STACK_NAME
#undef CONCURRENT_STACK

#undef CONCURRENT_STACK_TYPE