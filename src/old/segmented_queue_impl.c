#ifndef SEGMENTED_QUEUE_TYPE
    #error There must be a provided type for SEGMENTED_QUEUE_TYPE and it must be defined prior to using this code!
    #define SEGMENTED_QUEUE_TYPE int32_t // To make VSCode happy
#endif

#ifndef INDEX_TYPE
    #define INDEX_TYPE uint64_t
#endif

#ifndef STL_FOR_C_IMPLEMENTATION_ONLY
    #define STL_FOR_C_IMPLEMENTATION_ONLY
#endif

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "include/list_intptr_t.h"

#define MAKE_SEGMENT_NAME(x) segmented_queue_segment_ ## x
#define SEGMENT_NAME(x) MAKE_SEGMENT_NAME(x)
#define SEGMENT SEGMENT_NAME(SEGMENTED_QUEUE_TYPE)

#define MAKE_SEGMENTED_QUEUE_NAME(x) segmented_queue_ ## x
#define SEGMENTED_QUEUE_NAME(x) MAKE_SEGMENTED_QUEUE_NAME(x)
#define SEGMENTED_QUEUE SEGMENTED_QUEUE_NAME(SEGMENTED_QUEUE_TYPE)

#ifndef DEFAULT_SEGMENT_SIZE
    #define DEFAULT_SEGMENT_SIZE 32
#endif

typedef struct SEGMENT {
    SEGMENTED_QUEUE_TYPE* items;
    INDEX_TYPE low;
    INDEX_TYPE high;
} SEGMENT;

typedef struct SEGMENTED_QUEUE {
    list_intptr_t* segments;
} SEGMENTED_QUEUE;

#define MAKE_SEGMENT_NEW_NAME(x) x ## _new()
#define GEN_NEW_NAME(x) MAKE_SEGMENT_NEW_NAME(x)

SEGMENT* GEN_NEW_NAME(SEGMENT)
{
    SEGMENT* newSegment = (SEGMENT*)malloc(sizeof(SEGMENT));
    if (newSegment == NULL)
        return NULL;
    newSegment->items = (SEGMENTED_QUEUE_TYPE*)malloc(DEFAULT_SEGMENT_SIZE * sizeof(SEGMENTED_QUEUE_TYPE));
    if (newSegment->items == NULL)
    {
        free(newSegment);
        return NULL;
    }
    newSegment->low = 0;
    newSegment->high = 0;
    return newSegment;
}

#undef GEN_NEW_NAME
#undef MAKE_SEGMENT_NEW_NAME

#define MAKE_SEGMENT_DESTROY_NAME(x) x ## _destroy(x* this)
#define GEN_DESTROY_NAME(x) MAKE_SEGMENT_DESTROY_NAME(x)
bool GEN_DESTROY_NAME(SEGMENT)
{
    if (this == NULL)
        return true;
    if (this->items != NULL)
        free(this->items);
    free(this);
    return false;
}
#undef GEN_DESTROY_NAME
#undef MAKE_SEGMENT_DESTROY_NAME

#define MAKE_SEGMENT_PUSH_NAME(x, y, z) x ## _push(x* this, y item, z* outEnsegmented_queuedCount)
#define GEN_PUSH_NAME(x, y, z) MAKE_SEGMENT_PUSH_NAME(x, y, z)

bool GEN_PUSH_NAME(SEGMENT, SEGMENTED_QUEUE_TYPE, INDEX_TYPE)
{
    if (this == NULL || this->items == NULL)
        return true;
    if (this->high >= DEFAULT_SEGMENT_SIZE)
    {
        *outEnsegmented_queuedCount = 0;
        return false;
    }
    this->items[this->high++] = item;
    *outEnsegmented_queuedCount = 1;
    return false;
}

#undef GEN_PUSH_NAME
#undef MAKE_SEGMENT_PUSH_NAME

#define MAKE_SEGMENT_PUSH_ITEMS_NAME(x, y, z) x ## _push_items(x* this, y* items, z items_index, z items_count, z* outEnsegmented_queuedCount)
#define GEN_PUSH_ITEMS_NAME(x, y, z) MAKE_SEGMENT_PUSH_ITEMS_NAME(x, y, z)

bool GEN_PUSH_ITEMS_NAME(SEGMENT, SEGMENTED_QUEUE_TYPE, INDEX_TYPE)
{
    if (this == NULL || this->items == NULL || items == NULL)
        return true;
    if (this->high >= DEFAULT_SEGMENT_SIZE || items_count <= 0)
    {
        *outEnsegmented_queuedCount = 0;
        return false;
    }
    INDEX_TYPE itemsToEnsegmented_queueCount = DEFAULT_SEGMENT_SIZE - this->high;
    if (itemsToEnsegmented_queueCount > items_count)
        itemsToEnsegmented_queueCount = items_count;
    
    memcpy(&this->items[this->high], &items[items_index], itemsToEnsegmented_queueCount * sizeof(SEGMENTED_QUEUE_TYPE));
    this->high += itemsToEnsegmented_queueCount;

    *outEnsegmented_queuedCount = itemsToEnsegmented_queueCount;
    return false;
}

#undef GEN_PUSH_ITEMS_NAME
#undef MAKE_SEGMENT_PUSH_ITEMS_NAME

#define MAKE_SEGMENT_POP_NAME(x, y) x ## _pop(x* this, y* outItem)
#define GEN_POP_NAME(x, y) MAKE_SEGMENT_POP_NAME(x, y)
bool GEN_POP_NAME(SEGMENT, SEGMENTED_QUEUE_TYPE)
{
    if (this == NULL || this->items == NULL || this->low > this->high)
        return true;
    *outItem = this->items[this->low++];
    return false;
}
#undef GEN_POP_NAME
#undef MAKE_SEGMENT_POP_NAME

#define MAKE_SEGMENT_POP_ITEMS_NAME(x, y, z) x ## _pop_items(x* this, z count, y* outItems, z* outCount)
#define GEN_POP_ITEMS_NAME(x, y, z) MAKE_SEGMENT_POP_ITEMS_NAME(x, y, z)

bool GEN_POP_ITEMS_NAME(SEGMENT, SEGMENTED_QUEUE_TYPE, INDEX_TYPE)
{
    if (this == NULL || this->items == NULL || this->low > this->high)
        return true;
    INDEX_TYPE itemsToPopCount = this->high - this->low;
    if (itemsToPopCount > count)
        itemsToPopCount = count;
    memcpy(outItems, &this->items[this->low], itemsToPopCount * sizeof(SEGMENTED_QUEUE_TYPE));
    this->low += itemsToPopCount;
    *outCount = itemsToPopCount;
    return false;
}

#undef GEN_POP_ITEMS_NAME
#undef MAKE_SEGMENT_POP_ITEMS_NAME

#define MAKE_SEGMENT_GETCOUNT_NAME(x, y) x ## _getcount(x* this, y* outCount)
#define GEN_GETCOUNT_NAME(x, y) MAKE_SEGMENT_GETCOUNT_NAME(x, y)

bool GEN_GETCOUNT_NAME(SEGMENT, INDEX_TYPE)
{
    if (this->low > this->high)
    {
        *outCount = 0;
        return false;
    }
    *outCount = this->high - this->low;
    return false;
}

#undef GEN_GETCOUNT_NAME
#undef MAKE_SEGMENT_GETCOUNT_NAME

#define MAKE_SEGMENT_REMAINING_CAPACITY_NAME(x, y) x ## _remaining_capacity(x* this, y* outRemainingCapacityCount)
#define GEN_REMAINING_CAPACITY_NAME(x, y) MAKE_SEGMENT_REMAINING_CAPACITY_NAME(x, y)

bool GEN_REMAINING_CAPACITY_NAME(SEGMENT, INDEX_TYPE)
{
    if (this->high >= DEFAULT_SEGMENT_SIZE)
    {
        *outRemainingCapacityCount = 0;
        return false;
    }

    *outRemainingCapacityCount = DEFAULT_SEGMENT_SIZE - this->high;
    return false;
}

#undef GEN_REMAINING_CAPACITY_NAME
#undef MAKE_SEGMENT_REMAINING_CAPACITY_NAME

#define MAKE_SEGMENT_RESET_NAME(x) x ## _reset(x* this)
#define GEN_RESET_NAME(x) MAKE_SEGMENT_RESET_NAME(x)

bool GEN_RESET_NAME(SEGMENT)
{
    if (this == NULL)
        return true;
    this->high = 0;
    this->low = 0;
    return false;
}
#undef GEN_RESET_NAME
#undef MAKE_SEGMENT_RESET_NAME

#define MAKE_SEGMENTED_QUEUE_NEW_NAME(x) x ## _new()
#define GEN_NEW_NAME(x) MAKE_SEGMENTED_QUEUE_NEW_NAME(x)

SEGMENTED_QUEUE* GEN_NEW_NAME(SEGMENTED_QUEUE)
{
    #define MAKE_SEGMENT_NEW_NAME(x) x ## _new
    #define GEN_SEGMENT_NEW_NAME(x) MAKE_SEGMENT_NEW_NAME(x)
    
    SEGMENTED_QUEUE* newQueue = (SEGMENTED_QUEUE*)malloc(sizeof(SEGMENTED_QUEUE));
    if (newQueue == NULL)
        return NULL;
    newQueue->segments = list_intptr_t_new();
    if (newQueue->segments == NULL)
    {
        free(newQueue);
        return NULL;
    }
    SEGMENT* newSegment = GEN_SEGMENT_NEW_NAME(SEGMENT)();
    if (newSegment == NULL)
    {
        list_intptr_t_destroy(newQueue->segments);
        free(newQueue);
        return NULL;
    }
    list_intptr_t_add(newQueue->segments, (intptr_t)newSegment);
    return newQueue;

    #undef GEN_SEGMENT_NEW_NAME
    #undef MAKE_SEGMENT_NEW_NAME
}

#undef GEN_NEW_NAME
#undef MAKE_SEGMENTED_QUEUE_NEW_NAME

#define MAKE_SEGMENTED_QUEUE_DESTROY_NAME(x) x ## _destroy(SEGMENTED_QUEUE* this)
#define GEN_DESTROY_NAME(x) MAKE_SEGMENTED_QUEUE_DESTROY_NAME(x)

bool GEN_DESTROY_NAME(SEGMENTED_QUEUE)
{
    #define MAKE_SEGMENT_DESTROY_NAME(x) x ## _destroy
    #define GEN_SEGMENT_DESTROY_NAME(x) MAKE_SEGMENT_DESTROY_NAME(x)
    if (this == NULL)
        return true;
    printf("%p\n", this);
    printf("%p\n", this->segments);
    for (INDEX_TYPE i = 0; i < this->segments->size; ++i)
    {
        GEN_SEGMENT_DESTROY_NAME(SEGMENT)((SEGMENT*)this->segments->items[i]);
    }
    list_intptr_t_destroy(this->segments);
    free(this);
    return false;

    #undef GEN_SEGMENT_DESTROY_NAME
    #undef MAKE_SEGMENT_DESTROY_NAME
}

#undef GEN_DESTROY_NAME
#undef MAKE_SEGMENTED_QUEUE_DESTROY_NAME

#define MAKE_SEGMENTED_QUEUE_CLEAR_NAME(x) x ## _clear(SEGMENTED_QUEUE* this)
#define GEN_CLEAR_NAME(x) MAKE_SEGMENTED_QUEUE_CLEAR_NAME(x)

bool GEN_CLEAR_NAME(SEGMENTED_QUEUE)
{
    #define MAKE_SEGMENT_RESET_NAME(x) x ## _reset
    #define GEN_SEGMENT_RESET_NAME(x) MAKE_SEGMENT_RESET_NAME(x)
    INDEX_TYPE count = 0;
    if (list_intptr_t_count(this->segments, &count))
        return true;
    if (count > 1 && list_intptr_t_removerange(this->segments, 1, this->segments->size - 1))
        return true;
    if (GEN_SEGMENT_RESET_NAME(SEGMENT)((SEGMENT*)&this->segments->items[0]))
        return true;
    return false;

    #undef GEN_SEGMENT_RESET_NAME
    #undef MAKE_SEGMENT_RESET_NAME
}

#undef GEN_CLEAR_NAME
#undef MAKE_SEGMENTED_QUEUE_CLEAR_NAME

#define MAKE_SEGMENTED_QUEUE_GETCOUNT_NAME(x) x ## _getcount(SEGMENTED_QUEUE* this, INDEX_TYPE* outCount)
#define GEN_GETCOUNT_NAME(x) MAKE_SEGMENTED_QUEUE_GETCOUNT_NAME(x)

bool GEN_GETCOUNT_NAME(SEGMENTED_QUEUE)
{
    if (this == NULL)
        return true;
    if (list_intptr_t_count(this->segments, outCount))
        return true;
    return false;
}

#undef GEN_GETCOUNT_NAME
#undef MAKE_SEGMENTED_QUEUE_GETCOUNT_NAME

#define MAKE_SEGMENTED_QUEUE_PUSH_NAME(x, y, z) x ## _push(x* this, y item, z* outEnsegmented_queuedCount)
#define GEN_PUSH_NAME(x, y, z) MAKE_SEGMENTED_QUEUE_PUSH_NAME(x, y, z)

bool GEN_PUSH_NAME(SEGMENTED_QUEUE, SEGMENTED_QUEUE_TYPE, INDEX_TYPE)
{
    #define MAKE_SEGMENT_PUSH_NAME(x) x ## _push
    #define GEN_SEGMENT_PUSH_NAME(x) MAKE_SEGMENT_PUSH_NAME(x)
    #define MAKE_SEGMENT_REMAINING_CAPACITY_NAME(x) x ## _remaining_capacity
    #define GEN_SEGMENT_REMAINING_CAPACITY_NAME(x) MAKE_SEGMENT_REMAINING_CAPACITY_NAME(x)
    #define MAKE_SEGMENT_NEW_NAME(x) x ## _new
    #define GEN_SEGMENT_NEW_NAME(x) MAKE_SEGMENT_NEW_NAME(x)

    if (this == NULL)
        return true;
    INDEX_TYPE capacity = 0;
    SEGMENT* lastSegment = NULL;
    INDEX_TYPE segCount = 0;
    if (list_intptr_t_count(this->segments, &segCount))
        return true;
    if (segCount >= 0)
    {
        list_intptr_t_get_last(this->segments, (intptr_t*) &lastSegment);
    }
    else
    {
        SEGMENT* segment = GEN_SEGMENT_NEW_NAME(SEGMENT)();
        if (segment == NULL)
            return true;
        if (list_intptr_t_add(this->segments, (intptr_t) segment))
            return true;
        GEN_SEGMENT_PUSH_NAME(SEGMENT)(segment, item, outEnsegmented_queuedCount);
        printf("Exited through here.\n");
        return false;
    }
    if (GEN_SEGMENT_REMAINING_CAPACITY_NAME(SEGMENT)(lastSegment, &capacity))
        return true;
    if (capacity <= 0)
    {
        SEGMENT* newSegment = GEN_SEGMENT_NEW_NAME(SEGMENT)();
        if (newSegment == NULL)
            return true;
        list_intptr_t_add(this->segments, (intptr_t) newSegment);
        lastSegment = newSegment;
    }
    GEN_SEGMENT_PUSH_NAME(SEGMENT)(lastSegment, item, outEnsegmented_queuedCount);
    if (*outEnsegmented_queuedCount != 1)
        return true;
    return false;

    #undef GEN_SEGMENT_PUSH_NAME
    #undef MAKE_SEGMENT_PUSH_NAME
    #undef GEN_SEGMENT_REMAINING_CAPACITY_NAME
    #undef MAKE_SEGMENT_REMAINING_CAPACITY_NAME
    #undef GEN_SEGMENT_NEW_NAME
    #undef MAKE_SEGMENT_NEW_NAME
}

#undef GEN_PUSH_NAME
#undef MAKE_SEGMENTED_QUEUE_PUSH_NAME

#define MAKE_SEGMENTED_QUEUE_PUSH_ITEMS_NAME(x, y, z) x ## _push_items(x* this, y* items, z items_index, z items_count, z* outEnsegmented_queuedCount)
#define GEN_PUSH_ITEMS_NAME(x, y, z) MAKE_SEGMENTED_QUEUE_PUSH_ITEMS_NAME(x, y, z)

bool GEN_PUSH_ITEMS_NAME(SEGMENTED_QUEUE, SEGMENTED_QUEUE_TYPE, INDEX_TYPE)
{
    #define MAKE_SEGMENT_PUSH_ITEMS_NAME(x) x ## _push_items
    #define GEN_SEGMENT_PUSH_ITEMS_NAME(x) MAKE_SEGMENT_PUSH_ITEMS_NAME(x)
    #define MAKE_SEGMENT_REMAINING_CAPACITY_NAME(x) x ## _remaining_capacity
    #define GEN_SEGMENT_REMAINING_CAPACITY_NAME(x) MAKE_SEGMENT_REMAINING_CAPACITY_NAME(x)
    #define MAKE_SEGMENT_NEW_NAME(x) x ## _new
    #define GEN_SEGMENT_NEW_NAME(x) MAKE_SEGMENT_NEW_NAME(x)

    if (this == NULL)
        return true;
    for (INDEX_TYPE remainingItems = items_count; remainingItems > 0;)
    {
        SEGMENT* lastSegment = NULL;
        INDEX_TYPE remainingCapacity = 0;
        if (list_intptr_t_get_last(this->segments, (intptr_t*) &lastSegment))
            return true;
        if (GEN_SEGMENT_REMAINING_CAPACITY_NAME(SEGMENT)(lastSegment, &remainingCapacity))
            return true;
        if (remainingCapacity <= 0)
        {
            SEGMENT* newSegment = GEN_SEGMENT_NEW_NAME(SEGMENT)();
            if (newSegment == NULL)
                return true;
            list_intptr_t_add(this->segments, (intptr_t) newSegment);
            lastSegment = newSegment;
        }
        INDEX_TYPE ensegmented_queuedCount = 0;
        if (GEN_SEGMENT_PUSH_ITEMS_NAME(SEGMENT)(lastSegment, items, items_index, remainingItems, &ensegmented_queuedCount))
            return true;
        remainingItems -= ensegmented_queuedCount;
        items_index += ensegmented_queuedCount;
    }
    return false;

    #undef GEN_SEGMENT_PUSH_ITEMS_NAME
    #undef MAKE_SEGMENT_PUSH_ITEMS_NAME
    #undef GEN_SEGMENT_REMAINING_CAPACITY_NAME
    #undef MAKE_SEGMENT_REMAINING_CAPACITY_NAME
    #undef GEN_SEGMENT_NEW_NAME
    #undef MAKE_SEGMENT_NEW_NAME
}

#undef GEN_PUSH_ITEMS_NAME
#undef MAKE_SEGMENTED_QUEUE_PUSH_ITEMS_NAME

#define MAKE_SEGMENTED_QUEUE_POP_NAME(x, y) x ## _pop(x* this, y* outItem)
#define GEN_POP_NAME(x, y) MAKE_SEGMENTED_QUEUE_POP_NAME(x, y)
bool GEN_POP_NAME(SEGMENTED_QUEUE, SEGMENTED_QUEUE_TYPE)
{
    #define MAKE_SEGMENT_POP_NAME(x) x ## _pop
    #define GEN_SEGMENT_POP_NAME(x) MAKE_SEGMENT_POP_NAME(x)
    #define MAKE_SEGMENT_GETCOUNT_NAME(x) x ## _getcount
    #define GEN_SEGMENT_GETCOUNT_NAME(x) MAKE_SEGMENT_GETCOUNT_NAME(x)
    #define MAKE_SEGMENT_REMAINING_CAPACITY_NAME(x) x ## _remaining_capacity
    #define GEN_SEGMENT_REMAINING_CAPACITY_NAME(x) MAKE_SEGMENT_REMAINING_CAPACITY_NAME(x)
    #define MAKE_SEGMENT_DESTROY_NAME(x) x ## _destroy
    #define GEN_SEGMENT_DESTROY_NAME(x) MAKE_SEGMENT_DESTROY_NAME(x)

    if (this == NULL)
        return true;
    INDEX_TYPE count = 0;
    INDEX_TYPE remainingCapacity = 0;
    SEGMENT* firstSegment = NULL;
    if (list_intptr_t_get_first(this->segments, (intptr_t*) &firstSegment))
        return true;
    
    if (GEN_SEGMENT_GETCOUNT_NAME(SEGMENT)(firstSegment, &count))
        return true;
    while (count <= 0)
    {
        if (GEN_SEGMENT_REMAINING_CAPACITY_NAME(SEGMENT)(firstSegment, &remainingCapacity))
            return true;
        if (remainingCapacity >= 0)
            return true;
        GEN_SEGMENT_DESTROY_NAME(SEGMENT)(firstSegment);
        if (list_intptr_t_remove(this->segments, 0))
            return true;
        INDEX_TYPE segmentCount = 0;
        if (list_intptr_t_count(this->segments, &segmentCount))
            return true;
        if (segmentCount <= 0)
            return true;
        if (list_intptr_t_get_first(this->segments, (intptr_t*) &firstSegment))
            return true;
        if (GEN_SEGMENT_GETCOUNT_NAME(SEGMENT)(firstSegment, &count))
            return true;
    }
    if (GEN_SEGMENT_POP_NAME(SEGMENT)(firstSegment, outItem))
        return true;
    return false;

    #undef GEN_DESTROY_NAME
    #undef MAKE_SEGMENT_DESTROY_NAME
    #undef GEN_SEGMENT_REMAINING_CAPACITY_NAME
    #undef MAKE_SEGMENT_REMAINING_CAPACITY_NAME
    #undef GEN_SEGMENT_POP_NAME
    #undef MAKE_SEGMENT_POP_NAME
    #undef GEN_SEGMENT_GETCOUNT_NAME
    #undef MAKE_SEGMENT_GETCOUNT_NAME
}
#undef GEN_POP_NAME
#undef MAKE_SEGMENTED_QUEUE_POP_NAME

#define MAKE_SEGMENTED_QUEUE_POP_ITEMS_NAME(x, y, z) x ## _pop_items(x* this, z count, y* outItems, z* outCount)
#define GEN_POP_ITEMS_NAME(x, y, z) MAKE_SEGMENTED_QUEUE_POP_ITEMS_NAME(x, y, z)

bool GEN_POP_ITEMS_NAME(SEGMENTED_QUEUE, SEGMENTED_QUEUE_TYPE, INDEX_TYPE)
{
    #define MAKE_SEGMENT_POP_ITEMS_NAME(x) x ## _pop_items
    #define GEN_SEGMENT_POP_ITEMS_NAME(x) MAKE_SEGMENT_POP_ITEMS_NAME(x)
    #define MAKE_SEGMENT_GETCOUNT_NAME(x) x ## _getcount
    #define GEN_GETCOUNT_NAME(x) MAKE_SEGMENT_GETCOUNT_NAME(x)
    #define MAKE_SEGMENT_REMAINING_CAPACITY_NAME(x) x ## _remaining_capacity
    #define GEN_SEGMENT_REMAINING_CAPACITY_NAME(x) MAKE_SEGMENT_REMAINING_CAPACITY_NAME(x)
    #define MAKE_SEGMENT_DESTROY_NAME(x) x ## _destroy
    #define GEN_SEGMENT_DESTROY_NAME(x) MAKE_SEGMENT_DESTROY_NAME(x)

    if (this == NULL) return true;
    INDEX_TYPE remainingCount = count;
    for (; remainingCount >= 0;)
    {
        INDEX_TYPE segCount = 0;
        if (list_intptr_t_count(this->segments, &segCount))
            return true;
        if (segCount <= 0)
            break;
        SEGMENT* firstSegment = NULL;
        if (list_intptr_t_get_first(this->segments, (intptr_t*)&firstSegment))
            return true;
        INDEX_TYPE remainingCapacity = 0;
        INDEX_TYPE remainingCount = 0;
        if (GEN_SEGMENT_REMAINING_CAPACITY_NAME(SEGMENT)(firstSegment, &remainingCapacity))
            return true;
        if (GEN_GETCOUNT_NAME(SEGMENT)(firstSegment, &remainingCount))
            return true;
        if (remainingCapacity <= 0 && remainingCount <= 0)
        {
            GEN_SEGMENT_DESTROY_NAME(SEGMENT)(firstSegment);
            if (list_intptr_t_remove(this->segments, 0))
                return true;
            continue;
        }
        INDEX_TYPE poppedCount = 0;
        if (GEN_SEGMENT_POP_ITEMS_NAME(SEGMENT)(firstSegment, remainingCount, &outItems[count - remainingCount], &poppedCount))
            return true;
        remainingCount -= poppedCount;
    }
    *outCount = count - remainingCount;
    return false;

    #undef GEN_SEGMENT_DESTROY_NAME
    #undef MAKE_SEGMENT_DESTROY_NAME
    #undef GEN_SEGMENT_REMAINING_CAPACITY_NAME
    #undef MAKE_SEGMENT_REMAINING_CAPACITY_NAME
    #undef GEN_SEGMENT_POP_NAME
    #undef MAKE_SEGMENT_POP_NAME
    #undef GEN_SEGMENT_GETCOUNT_NAME
    #undef MAKE_SEGMENT_GETCOUNT_NAME
}

#undef GEN_POP_ITEMS_NAME
#undef MAKE_SEGMENT_POP_ITEMS_NAME

#undef STL_FOR_C_IMPLEMENTATION_ONLY
#undef INDEX_TYPE
#undef SEGMENTED_QUEUE_TYPE
#undef SEGMENT
#undef SEGMENT_NAME
#undef MAKE_SEGMENT_NAME
#undef SEGMENTED_QUEUE
#undef SEGMENTED_QUEUE_NAME
#undef MAKE_SEGMENTED_QUEUE_NAME
#undef DEFAULT_SEGMENT_SIZE