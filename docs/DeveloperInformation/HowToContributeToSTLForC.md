## How to contribute to STL To C project?

There are generally two branches to be aware of in STL-For-C, **staging** and **main** branches. When you make a new pull request, please make sure to have pull request merge into **staging** branch. We operates a private CI/CD server and one of the consideration we make is security, so the process for Pull Request is as followed:

1. Create Pull Request for Staging branch
2. Contributor will review your Pull Request and will approve/reject with reasonings
3. Once your pull request is merged into Staging branch, CI/CD will be triggered automatically
4. CI/CD will pull from repo, build and run `meson test -C build` and `cppcheck`
5. If any issue is generated from the CI/CD, it will open an issue and provide a reference to it in a comment on your Pull Request
    
    5.1. You can make further Pull Requests to correct the issues or contributors can correct the issues, we are more than happy to work with you
    
    5.2. If the issue is significant, contributors will rollback the merge in staging branch and will try to provide you guidance on how to correct the issues for future pull request.

6. if everything is passing without issues from CI/CD and passed contributors reviews, your code will make it to the main branch!

The main branch is intended to be the stable branch, so if there is any pending issues in staging then it will not be merged into main branch until it is fixed.

We also encourage you to add copyright information in License text file with your name and date of contribution so that your work can be credited.
