There are generally two approaches to how you can utilize each data container types in your project:

#### Replace Text Method

#### Header Inclusion Method

The recommended practice in this method is to create a header file that will also include relevant data container header files with defined variables for each container in a header file as followed:

###### MyLists.h
```c
#ifndef MY_LIST_H
#define MY_LIST_H

#include <stdint.h>

#define LIST_TYPE int64_t
#include "STL_For_C/include/list.h"

#define LIST_TYPE int32_t
#include "STL_For_C/include/list.h"

// etc etc etc

#endif
```

This approach is preferred, because each data container header files **does not** have any header guard to prevent duplicate listing, so this must be strictly guarded by creating a separate header files that uses those header files.

After creating the header file above, you must define an implementation file as followed:

###### MyList.c
```c
#define LIST_TYPE int64_t
#include "STL_For_C/src/list.h"

#define LIST_TYPE int32_t
#include "STL_For_C/src/list.h"
```

And ensure that MyList.c is linked in with the rest of your program.

Now you can use the newly created list for your project like so for an example:

###### main.c
```c
#include "MyList.h"

int main()
{
    list_int32_t* list = list_int32_t_new();
    // etc etc etc
    list_int32_t_destroy(list);
    return 0;
}
```

The only drawback with this method is the lack of documentation for each generated functions due to the limitation of preprocessing, this why the search/replace method is superior albeit it takes extra effort.
