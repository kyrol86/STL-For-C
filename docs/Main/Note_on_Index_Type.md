TL;DR: Don't change INDEX_TYPE, but if you do, make sure it is consistently defined the same across all types in STL for C to avoid stack corruption.

When modifying the default INDEX_TYPE definition in C header and source files for this project, it is recommended to keep INDEX_TYPE consistent across all STL for C types otherwise stack corruption may occurs when one generic type uses the other generic type.
