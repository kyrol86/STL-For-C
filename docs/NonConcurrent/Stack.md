# Non-Concurrent Stack

## Tutorial

1. Create three files: `stack_int32_t.h`, `stack_int32_t_impl.c`, `main.c` and then `git clone https://codeberg.org/CLanguagePurist/STL-For-C` for the demonstrative purpose
2. In `stack_int32_t.h`, it should have a header guard, have STACK_TYPE variable defined, and include the header code for stack:
```c
#ifndef STACK_INT32_T_H
#define STACK_INT32_T_H

#include <stdint.h> // For int32_t definition

#define STACK_TYPE int32_t
#include "STL_For_C/include/stack.h"

#endif
```

3. in `stack_int32_t_impl.c`, it should define STACK_TYPE variable and include stack implementation:
```c
#include <stdint.h>
#define STACK_TYPE int32_t
#include "STL_For_C/src/stack_impl.c"
```

4. Finally in `main.c`, you can use various functions provided by stack type as followed:

```c
#include "stack_int32_t.h"
#include <stdlib.h>
#include <stdio.h>

int main()
{
    stack_int32_t* stack = stack_int32_t_new();
    if (stack == NULL) {
        printf("Failed to allocate a stack!\n");
        return 1;
    }
    if (stack_int32_t_push(stack, 1)) {
        printf("Failed to push item to stack!\n");
        return 1;
    }
    int32_t item = 0;
    if (stack_int32_t_pop(stack, &item)) {
        printf("Failed to pop item from stack!\n");
        return 1;
    }
    if (item != 1)
    {
        printf("Destackd item does not match with push item!\n");
        return 1;
    }
    printf("Success!\n");
    return 0;
}
```

5. Run: `CC -oMyProgram -I. stack_int32_t_impl.c main.c`


## FIles ##
* STL_For_C/include/stack.h
* STL_For_C/src/stack_impl.c

## Variables
| Variable Name | Description |
| :-- | :-- |
| **STACK_TYPE** | Define a type of element in a stack to be preprocessed. <br/> **Example:** ` #define STACK_TYPE int32_t ` <br/> **Note:** The type name should not contains any space or asterisk otherwise a preprocessing error results, use typedef instead. <br /> **DO:** <br/> `typedef int32_t* ptrToInt32_t;` <br/> `#define STACK_TYPE ptrToInt32_t` <br /> **DON'T:** <br/> `#define STACK_TYPE int32_t*` |
| **INDEX_TYPE** | Define an integer or floating point type for List to utilize for keeping track of index and sizes. This defaults to uint64_t unless specify otherwise. Struct should not be used for this type and that it should either be a floating point or integer that can be computed with arithmetic. <br/> **DO:** <br/>`#define INDEX_TYPE int32_t` <br/> **DON'T**: <br/> `typedef struct { float a; } MyStruct;` <br/> `#define INDEX_TYPE MyStruct` <br/> **WARNING:** Integer underflow or overflow is still a risk if you chose to use smaller sized data types and if floating point is chosen for index it may results in data corruption, so evaluate whether INDEX_TYPE should be changed with this in mind, if this is to be changed, make sure you keep INDEX_TYPE consistent across all data containers since other data containers may use underlying implementation. Both unsigned and signed integer can be used. |
| **MINIMUM_STACK_SIZE** | Define the minimum size as an integer or floating point value. This defaults to 32 items capacity whenever stack is being allocated without capacity being specified or when clearing the stack that will results in reallocating the stack back to 32 items capacity. It's recommended to leave this unchanged. <br/> **DO:** <br/> `#define MINIMUM_LIST_SIZE 32` <br/> **DON'T:** <br/> `#define MINIMUM_LIST_SIZE -1` <br/> **or** <br/> `#define MINIMUM_LIST_SIZE int` <br/> **WARNING:** If specified to 0 or below items capacity then arthimetric and memory allocation errors will result. |
| **ALLOW_SHRINKING_STACK** | Define whether to allow stack to be reallocated when popd sufficient amount of elements from stack. By default, shrinking stack will only trigger when size of items in stack is below or equals to the quarter of capacity, then resized, the capacity will be halved. This is set to 0 by default if not defined. This should be defined to a 1 or an 0. <br/> **DO:** <br/> `#define ALLOW_SHRINKING_STACK 1` <br/> **or** <br/> `#define ALLOW_SHRINKING_STACK 0` <br/> **DON'T:** <br/> `#define ALLOW_SHRINKING_STACK 2` |

## Functions

Replace X for STACK_TYPE for the followings:

| Functions | Description |
| :-- | :-- |
| <span style="color: red;">stack_X*</span> stack_X_new() | **Returns:** <span style="color:red;"> stack_X* </span> - New allocated stack object <br/> **Description:** Allocates a new stack_X object with the default size of 32 or specified MINIMUM_STACK_SIZE variable items capacity. Returns NULL if fail to allocate new stack_X object. |
| <span style="color: red;">stack_X*</span> stack_X_new_withcapacity(<span style="color:green;">INDEX_TYPE initialCapacity</span>) | **Parameter 1:** <span style="color:green;">INDEX_TYPE initialCapacity</span> - Number of elements size capacity that stack should be allocated for, must exceed MINIMUM_STACK_SIZE variable otherwise MINIMUM_STACK_SIZE will be used instead of initialCapacity parameter. <br/> **Returns:** <span style="color: red;">stack_X*</span> - New allocated stack object with specified capacity or MINIMUM_STACK_SIZE capacity. <br/> **Description:** Allocates a new stack_X object with the specified initial capacity or MINIMUM_STACK_SIZE variable capacity. Returns NULL if fail to allocate new stack-X object. |
| <span style="color: red;">bool</span> stack_X_destroy(<span style="color:green;">stack_X* this</span>) | **Parameter 1:** <span style="color:green;">stack_X* this</span> - Allocated stack_X object to be destroyed. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error when disposing `this` parameter, otherwise false on successful disposal. <br/> **Description:** Dispose allocated stack_X object and return false on successful disposal. Returns true if an error occurs during disposal. Elements stored in `this` stack will not be disposed, those memories must be managed separately. |
| <span style="color:red;">bool</span> stack_X_clear(<span style="color:green;">stack_X* this</span>) | **Parameter 1:** <span style="color:green;">stack_X* this</span> - Allocated stack_X object to be cleared. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error when clearing `this` stack, otherwise returns false on success. <br/> **Description:** Clear `this` stack by reallocating the stack to MINIMUM_STACK_SIZE capacity and set the size of stack to zero. Memory for each element stored in this stack must be managed separately. Returns true on error, otherwise returns false on success. |
| <span style="color:red;">bool</span> stack_X_getcount(<span style="color:green;">stack_X* this</span>, <span style="color:purple;">INDEX_TYPE* outCount</span>) | **Parameter 1:** <span style="color:green;">stack_X* this</span> - Allocated stack_X object to get count of elements from. <br/> **Parameter 2:** <span style="color:purple;">INDEX_TYPE* outCount</span> - Pointer to INDEX_TYPE variable to write count of elements in stack to. <br/> **Returns:** Returns true if either `this` parameter or `outCount` parameter are NULL or invalid, otherwise returns false on success. <br/> **Description:** Obtain a count of elements stored in `this` stack object and write the count of elements in stack to `outCount` pointer. Returns true on error if either `this` or `outCount` are NULL or invalid, otherwise returns false on success. |
| <span style="color:red;">bool</span> stack_X_reallocate(<span style="color:green;">stack_X* this</span>, <span style="color:purple;">INDEX_TYPE newSize</span>) | **Parameter 1:** <span style="color:green;">stack_X* this</span> - Allocated stack_X object to be reallocated. <br/> **Parameter 2:** <span style="color:purple;">INDEX_TYPE newSize</span> - New capacity size to reallocate `this` stack to. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error during reallocation of `this` stack, otherwise returns false on success. <br/> **Description:** Reallocate `this` stack by specified `newSize` parameter and the size specified must exceed the current stored number of elements in the stack. The elements stored in stack will be rearranged to allows for capacity reallocation, so a performance penalty during reallocation can occurs. Returns true on error, otherwise returns false on success. |
| <span style="color:red">bool</span> stack_X_push(<span style="color:green;">stack_X* this</span>, <span style="color:purple;">X item</span>, <span style="color:teal;">INDEX_TYPE* outEnstackdCount</span>) | **Parameter 1:** <span style="color:green;">stack_X* this</span> - Allocated stack_X object to be pushd to. <br/> **Parameter 2:** <span style="color:purple;">X item</span> - Item to be pushd into `this` stack. <br/> **Parameter 3:** <span style="color:teal;">INDEX_TYPE* outEnstackdCount</span> - An optional out parameter to store count of pushd item, can be NULL. <br/> **Returns:** <span style="color:red">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Enstack `item` into `this` stack and return the count of push item that was successfully added to stack in `outEnstackdCount`. `outEnstackdCount` can be NULL if pushd item count is not desired. If item exceed capacity of `this` stack, the stack may automatically reallocate to accommodate a new entry. Returns true on error, otherwise returns false on success. |
| <span style="color:red">bool</span> stack_X_push_items(<span style="color:green;">stack_X* this</span>, <span style="color:purple;">X* items</span>, <span style="color:teal;">INDEX_TYPE items_index</span>, <span style="color:orange;">INDEX_TYPE items_length</span>, <span style="color:brown;">INDEX_TYPE* outEnstackdCount</span>) | **Parameter 1:** <span style="color:green;">stack_X* this</span> - Allocated stack_X object to be pushd to. <br/> **Parameter 2:** <span style="color:purple;">X* items</span> - Pointer to memory containing a range of elements to be pushd to stack. <br/> **Parameter 3:** <span style="color:teal;">INDEX_TYPE items_index</span> - The starting index of items to be pushd, if items contains valid starting point, then this should be set to 0. <br/> **Parameter 4:** <span style="color:orange;">INDEX_TYPE items_length</span> - Number of elements to be pushd on and after `items_index` index. <br/> **Parameter 5:** <span style="color:brown;">INDEX_TYPE* outEnstackdCount</span> - An optional out parameter to store count of pushd item, can be NULL. <br/> **Returns:** <span style="color:red">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Enstack a range of `item` defined by `items_index` and `items_length` into `this` stack and returns the count of pushd item that was successfully added to stack in `outEnstackdCount`. `outEnstackdCount` can be NULL if pushd item count is not desired. If item exceed capacity of `this` stack, the stack may automatically reallocate to accommodate a new entry. Returns true on error, otherwise returns false on success. |
| <span style="color:red;">bool</span> stack_X_pop(<span style="color:green;">stack_X* this</span>, <span style="color:purple;">X* outItem</span>, <span style="color:teal;">INDEX_TYPE* outDestackCount</span>) | **Parameter 1:** <span style="color:green;">stack_X* this</span> - Allocated stack_X object to pop from. <br/> **Parameter 2:** <span style="color:purple;">X* outItem</span> - Pointer to variable for pop item to be written into. <br/> **Parameter 3:** <span style="color:teal;">INDEX_TYPE* outDestackCount</span> - An optional pointer to INDEX_TYPE to write number of successful pop counts to, can be set to NULL. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Destack an item from `this` stack and write the popd item into `outItem` parameter and write the count of successful pop count into `outDestackCount` parameter if not NULL. Returns true on error, otherwise return false for success. |
| <span style="color:red;">bool</span> stack_X_pop_items(<span style="color:green;">stack_X* this</span>, <span style="color:purple;">INDEX_TYPE count</span>, <span style="color:teal;">X* outItems</span>, <span style="color:orange;">INDEX_TYPE* outDestackCount</span>) | **Parameter 1:** <span style="color:green;">stack_X* this</span> - Allocated stack_X object to pop from. <br/> **Parameter 2:** <span style="color:purple;">INDEX_TYPE count</span> - Number of items to be popd from `this` stack. <br/> **Parameter 3:** <span style="color:teal;">X* outItems</span> - Pointer to memory containing elements for popd items to be written into. <br/> **Parameter 4:** <span style="color:orange;">INDEX_TYPE* outDestackCount</span> - An optional pointer to INDEX_TYPE to write number of successful pop counts to, can be set to NULL. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Destack a `count` of items from `this` stack and write the popd items range into `outItems` parameter and write the count of successful pop count into `outDestackCount` parameter if not NULL. Returns true on error, otherwise return false for success. |
| <span style="color:red;">bool</span> stack_X_peek(<span style="color:green;">stack_X* this</span>, <span style="color:purple;">X* outItem</span>, <span style="color:teal;">INDEX_TYPE* outPeekCount</span>) | **Parameter 1:** <span style="color:green;">stack_X* this</span> - Allocated stack_X object to peek into. <br/> **Parameter 2:** <span style="color:purple;">X* outItem</span> - Pointer to variable for peek item to be written into. <br/> **Parameter 3:** <span style="color:teal;">INDEX_TYPE* outPeekCount</span> - An optional pointer to INDEX_TYPE to write number of successful peek counts to, can be set to NULL. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Peek into `this` stack and obtain an item without advancing the stack and write the peeked item into `outItem` parameter and write the count of successful peek count into `outPeekCount` parameter if not NULL. Returns true on error, otherwise return false for success. |
| <span style="color:red;">bool</span> stack_X_peek_items(<span style="color:green;">stack_X* this</span>, <span style="color:purple;">INDEX_TYPE count</span>, <span style="color:teal;">X* outItems</span>, <span style="color:orange;">INDEX_TYPE* outPeekCount</span>) | **Parameter 1:** <span style="color:green;">stack_X* this</span> - Allocated stack_X object to peek into. <br/> **Parameter 2:** <span style="color:purple;">INDEX_TYPE count</span> - Number of items to be peek into `this` stack. <br/> **Parameter 3:** <span style="color:teal;">X* outItems</span> - Pointer to memory containing elements for peek items to be written into. <br/> **Parameter 4:** <span style="color:orange;">INDEX_TYPE* outPeekCount</span> - An optional pointer to INDEX_TYPE to write a number of successful peek counts to, can be set to NULL. <br/> **Returns:** <span style="color:red;">bool</span> - Returns true on error, otherwise return false on success. <br/> **Description:** Peek `count` of items into `this` stack without advancing the stack and write the peeked items range into `outItems` parameter and write the count of successful peeked count into `outPeekCount` parameter if not NULL. Returns true on error, otherwise return false for success. |
## Implementation Specific Notes
:information_source: This implementation of Stack use circular buffer internally to minimize the use of reallocation. The implementation make extensive use of memcpy for most of it's SIMD optimization giving you the best performance result comparable to other implementation of stack.

:warning: It should be noted that anytime an element is added to the stack exceeding it's capacity, it will reallocates doubling the size of capacity. When the capacity exceed quarter of elements after removing items, it will also reallocate to shrink capacity halving it's size. 

Thread Safety: ❌  - This implementation is not meant to be thread safe, you will need to refer to [concurrent_stack](TODO) for thread safety.
