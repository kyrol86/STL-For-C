# List/Vector Non-Concurrent

## Tutorial

1. Create three files: `list_int32_t.h`, `list_int32_t_impl.c`, `main.c` and then `git clone https://codeberg.org/CLanguagePurist/STL-For-C` for the demonstrative purpose
2. In `list_int32_t.h`, it should have a header guard, have LIST_TYPE variable defined, and include the header code for list:
```c
#ifndef LIST_INT32_T_H
#define LIST_INT32_T_H

#include <stdint.h> // For int32_t definition

#define LIST_TYPE int32_t
#include "STL_For_C/include/list.h"

#endif
```

3. in `list_int32_t_impl.c`, it should define LIST_TYPE variable and include list implementation:
```c
#include <stdint.h>
#define LIST_TYPE int32_t
#include "STL_For_C/src/list_impl.c"
```

4. Finally in `main.c`, you can use various functions provided by list type as followed:

```c
#include "list_int32_t.h"
#include <stdlib.h>
#include <stdio.h>

int main()
{
    list_int32_t* list = list_int32_t_new();
    if (list == NULL) {
        printf("Failed to allocate a list!\n");
        return 1;
    }
    if (list_int32_t_add(list, 1)) {
        printf("Failed to add item to list!\n");
        return 1;
    }
    if (list_int32_t_remove_item(list, 1)) {
        printf("Failed to remove item from list!\n");
        return 1;
    }
    return 0;
}
```

5. Run: `CC -oMyProgram -I. list_int32_t_impl.c main.c`


## FIles ##
* STL_For_C/include/list.h
* STL_For_C/src/list_impl.c

## Variables
| Variable Name | Description |
| :-- | :-- |
| **LIST_TYPE** | Define a type of element in a List to be preprocessed. <br/> **Example:** ` #define LIST_TYPE int32_t ` <br/> **Note:** The type name should not contains any space or asterisk otherwise a preprocessing error results, use typedef instead. <br /> **DO:** <br/> `typedef int32_t* ptrToInt32_t;` <br/> `#define LIST_TYPE ptrToInt32_t` <br /> **DON'T:** <br/> `#define LIST_TYPE int32_t*` |
| **INDEX_TYPE** | Define an integer or floating point type for List to utilize for keeping track of index and sizes. This defaults to uint64_t unless specify otherwise. Struct should not be used for this type and that it should either be a floating point or integer that can be computed with arithmetic. <br/> **DO:** <br/>`#define INDEX_TYPE int32_t` <br/> **DON'T**: <br/> `typedef struct { float a; } MyStruct;` <br/> `#define INDEX_TYPE MyStruct` <br/> **WARNING:** Integer underflow or overflow is still a risk if you chose to use smaller sized data types and if floating point is chosen for index it may results in data corruption, so evaluate whether INDEX_TYPE should be changed with this in mind, if this is to be changed, make sure you keep INDEX_TYPE consistent across all data containers since other data containers may use underlying implementation. Both unsigned and signed integer can be used. |
| **MINIMUM_LIST_SIZE** | Define the minimum size as an integer or floating point value. This defaults to 32 items capacity whenever list is being allocated without capacity being specified or when clearing the list that will results in reallocating the list back to 32 items capacity. It's recommended to leave this unchanged. <br/> **DO:** <br/> `#define MINIMUM_LIST_SIZE 32` <br/> **DON'T:** <br/> `#define MINIMUM_LIST_SIZE -1` <br/> **or** <br/> `#define MINIMUM_LIST_SIZE int` <br/> **WARNING:** If specified to 0 or below items capacity then arthimetric and memory allocation errors will result. |

## Functions

Replace X for LIST_TYPE for the followings:

| Functions | Description |
| :-- | :-- |
| list_X* list_X_new() | **Returns:** list_X* - New allocated list object <br/> **Description:** Allocates a new list_X with the default size of 32 or specified MINIMUM_LIST_SIZE variable items capacity. |
| list_X* list_X_new_withcapacity(INDEX_TYPE initialCapacity) | **Parameter 1:** INDEX_TYPE initialCapacity - Initial allocation size of initialCapacity amount of items, if it is specified below MINIMUM_LIST_SIZE variable, it will be set to MINIMUM_LIST_SIZE value instead. <br/> **Returns:** list_X* - New allocated list object <br/> **Description:** Allocates a new list_X with the specified size from initialCapacity parameter for items capacity. |
| void list_X_destroy(list_X* this) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Returns:** None <br/> **Description:** Dispose any memory allocated by list_X object, it should be noted that it does not mean any memory resides in each element contained in the list to be disposed as well, those elements should be managed separately. |
| bool list_X_indexof(list_X* this, X searchItem, INDEX_TYPE* outIndex) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** X searchItem - A specific element item to be searched within List object provided by `this` parameter. <br/> **Parameter 3:** INDEX_TYPE* outIndex - Pointer to index type to be overwritten with an 0-based Index of item found, otherwise this index will not be written upon failure. <br/> **Returns:** bool - Returns true on failure to find object, index being specified out of range or `this` parameter being invalid, otherwise returns false on success. <br/>  **Description:** Attempts a linear search through the List type to obtain an 0-based index of found element within the list, otherwise returns true if item is not found within the list. |
| bool list_X_indexof2(list_X* this, X searchItem, INDEX_TYPE start, INDEX_TYPE* outIndex) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** X searchItem - A specific element item to be searched within List object provided by `this` parameter. <br/> **Parameter 3:** INDEX_TYPE start - An index within the List to begin the search from, if this exceed the range of List, the function will returns true for exceeding the range. <br/> **Parameter 4:** INDEX_TYPE* outIndex - Pointer to index type to be overwritten with an 0-based Index of item found, otherwise this index will not be written upon failure. <br/> **Returns:** bool - Returns true on failure to find object, index being specified out of range or `this` parameter being invalid, otherwise returns false for success. <br/> **Description:** Attempts a linear search through the List type starting from `start` parameter index to obtain an 0-based index of found element within the list and write into outIndex parameter, otherwise returns true if item is not found within the list. |
| bool list_X_indexof3(list_X* this, X searchItem, INDEX_TYPE start, INDEX_TYPE length, INDEX_TYPE* outIndex) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** X searchItem - A specific element item to be searched within List object provided by `this` parameter. <br/> **Parameter 3:** INDEX_TYPE start - An index within the List to begin the search from, if this exceed the range of List, the function will returns true for item not found. <br/> **Parameter 4:** INDEX_TYPE length - A count of elements ahead of `start` parameter creating a range of items to search through saving computational cost. <br/> **Parameter 5:** INDEX_TYPE* outIndex - Pointer to index type to be overwritten with an 0-based Index of item found, otherwise this index will not be written upon failure. <br/> **Returns:** bool - Returns true on failure to find object, index being specified out of range or `this` parameter being invalid, otherwise returns false on success. <br/> **Description:** Attempts a linear search through the List type starting from `start` parameter index and the `length` parameter creating a range of items to search through to obtain an 0-based index of found element within the list and write into outIndex parameter, otherwise returns true if item is not found within the list. |
| bool list_X_get(list_X* this, INDEX_TYPE index, LIST_TYPE* result) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** INDEX_TYPE index - Position within the list where specific element locates in. <br/> **Parameter 3:** LIST_TYPE* result - An out parameter where the content of result will be overwritten by the content of the element found from `index` parameter in the list. <br/> **Returns:** Returns true if an error occurs during it's attempt to obtain an element from the list, this may include Index being out of range, `result` parameter is NULL, or `this` parameter is NULL. Otherwise returns false on success. <br/> **Description:** Attempts an element lookup provided by `index` parameter and overwrite `result` out parameter. Returns true if an error occurs, otherwise false for successful operation. |
| bool list_X_getrange(list_X* this, INDEX_TYPE index, INDEX_TYPE length, X** result) |  **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** INDEX_TYPE start - An index within the List to begin the search from, if this exceed the range of List, the function will returns a -1 for item not found. <br/> **Parameter 3:** INDEX_TYPE length - Number of elements ahead of `start` index creating a range of elements to be retrieved from `this` list. <br/> **Parameter 4:** X** result - An output parameter to be written into. <br/> **Returns:** bool - Returns true on failure to obtain the list of elements from list or failure on allocation of memory for `result` parameter, otherwise returns false on success. <br/> **Description:** Attempts to retrieve specific range of elements within the list and allocate a new memory for `result` out parameter to contains the range of elements obtained from the list and overwrite `result` address. Returns true on failure, otherwise false on success. |
| bool list_X_add(list_X* this, X item) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** X item - Item to be added to the list <br/> **Returns:** Return true if an error occurs either by memory reallocation or invalid list, otherwise return false on success. <br/> **Description:** Add an `item` to the list, when item being added to the list exceed list's capacity, the list will resize the capacity to be twice the size of capacity size to accommodate new entry. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| bool list_X_addrange(list_X* this, X* items, INDEX_TYPE items_length) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** X items - Items to be added to the list <br/> **Parameter 3:** Number of elements in `items` parameter. <br/> **Returns:** Return true if an error occurs either by memory reallocation or invalid list, otherwise return false on success. <br/> **Description:** Add a specified amount (`items_length`) `items` to the list, when items being added to the list exceed list's capacity, the list will resize the capacity to be twice the size of capacity size or more depending on the items_length supplied to accommodate new entries. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| bool list_X_add_at(list_X* this, X item, INDEX_TYPE index) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** X item - Item to be added to the list <br/> **Parameter 3:** INDEX_TYPE index - Index to specify where to add an entry to the list. <br/> **Returns:** Return true if an error occurs either by memory reallocation or invalid list, otherwise return false on success. <br/> **Description:** Add an `item` to the list at a specified index in the list, when item being added to the list exceed list's capacity, the list will resize the capacity to be twice the size of capacity size to accommodate new entry. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| bool list_X_addrange_at(list_X* this, X* items, INDEX_TYPE items_length, INDEX_TYPE index) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** X items - Items to be added to the list <br/> **Parameter 3:** Number of elements in `items` parameter. <br/> **Parameter 4:** Index to specify where to put a set of items into the list. <br/> **Returns:** Return true if an error occurs either by memory reallocation, invalid index or invalid list, otherwise return false on success. <br/> **Description:** Add a specified amount (`items_length`) `items` to the list at a specified index, when items being added to the list exceed list's capacity, the list will resize the capacity to be twice the size of capacity size or more depending on the items_length supplied to accommodate new entries. If reallocation fails, this function will returns true for error on reallocation, invalid index or invalid list, otherwise returns false for successful operation. |
| bool list_X_remove(list_X* this, INDEX_TYPE index) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** INDEX_TYPE index - Index within the list for an item to be removed <br/> **Returns:** Return true if an error occurs either by memory reallocation, invalid index or invalid list, otherwise return false on success. <br/> **Description:** Remove an item from the list, when item being removed from the list caused the capacity to exceed 4 times the size of currently stored items in the list, the list will resize the capacity to be half the size of capacity size to reduce excessive memory usage. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| bool list_X_removerange(list_X* this, INDEX_TYPE index, INDEX_TYPE length) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** INDEX_TYPE index - Starting position of the range to be removed from list <br/> **Parameter 3:** INDEX_TYPE length - Number of elements to be removed ahead of `index` parameter. <br/> **Returns:** Return true if an error occurs either by memory reallocation, invalid index, invalid length or invalid list, otherwise return false on success. <br/> **Description:** Remove a specified amount (`length`) `items` from the list starting at `index`, when items being removed to the list caused capacity to exceed 4 times of the remaining number of elements stored in list, the list will resize the capacity to be half the size of capacity size or more depending on the `length`. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| bool list_X_remove_item(list_X* this, X item) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** X item - Item to be searched in the list to be removed <br/> **Returns:** Return true if an error occurs either by memory reallocation or if item could not be found in the list, otherwise return false on success. <br/> **Description:** Remove an item from the list, when item being removed from the list caused the capacity to exceed 4 times the size of currently stored items in the list, the list will resize the capacity to be half the size of capacity size to reduce excessive memory usage. If reallocation fails, this function will returns true for error on reallocation, otherwise returns false for successful operation. |
| bool list_X_clear(list_X* this) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Returns:** Return true if an error occurs if `this` parameter is invalid, otherwise return false on success. <br/> **Description:** Remove all items from the list and reallocate with 32 or MINIMUM_LIST_SIZE variable items capacity, any items removed must be freed separately. Returns true on invalid `this` parameter, otherwise return false on success. |
| bool list_X_count(list_X* this, INDEX_TYPE outCount) | **Parameter 1:** list_X* this - The allocated list referenced for this function. <br/> **Parameter 2:** INDEX_TYPE outCount - Pointer to INDEX_TYPE to write into the count of items stored in `this` list object. <br/> **Returns:** Returns number of items stored in this list. <br/> **Description:** Returns true on invalid `this` parameter, otherwise returns false on success and write a number of items stored in `this` list into `outCount` parameter. |

## Implementation Specific Notes
:warning: It should be noted that anytime an element is added to the List exceeding it's capacity, it will reallocates doubling the size of capacity. When the capacity exceed quarter of elements after removing items, it will also reallocate to shrink capacity halving it's size. 

Thread Safety: ❌  - This implementation is not meant to be thread safe, you will need to refer to [concurrent_list](TODO) for thread safety.
