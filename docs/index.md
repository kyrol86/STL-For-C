# STL For C
###### Written by [CLanguagePurist](https://codeberg.org/CLanguagePurist) and STL For C contributors

***
<span style='color:blue;'>Test</span>
## Documentation

* [Project Structure Guideline](Main/Project_Structure_Guideline.md)
* [Performance Benchmark]()
* [Note on INDEX_TYPE](Main/Note_on_Index_Type.md)

##### Non-Concurrent (Thread Unsafe)
* [List/Vector](NonConcurrent/List_Vector.md)
* [Dictionary/HashTable]()
* [Queue](NonConcurrent/Queue.md)
* [Stack](NonConcurrent/Stack.md)
* [Event]()
* [Hashset]()
* [Single Linked List]()
* [Doubly Linked List]()

##### Concurrent (Thread Safe)
* [List/Vector]()
* [Dictionary/HashTable]()
* [Queue]()
* [Stack]()
* [Event]()
* [Hashset]()
* [Single Linked List]()
* [Doubly Linked List]()

##### Future Data Container Types

* deque (Double Ended Queue)
* multiset
* multimap
* priority queue

##### Developer Information

* [How To Contribute to STL For C?](DeveloperInformation/HowToContributeToSTLForC.md)

Got an idea? Bring it up for [discussion](https://codeberg.org/CLanguagePurist/STL-For-C/issues)! 
