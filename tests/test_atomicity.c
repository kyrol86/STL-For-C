#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdatomic.h>
#include "tinycthread.h"

static volatile _Atomic int64_t globalValue = 0;
static volatile _Atomic int64_t completedCount = 0;

int ApplyOperationThread(void *arg)
{
    int64_t addorsub = ((int64_t*)arg)[0];
    int64_t count = ((int64_t*)arg)[1];
    int64_t val = globalValue;
    for (int64_t i = 0; i < count; ++i)
    {
        if (addorsub)
            while (!atomic_compare_exchange_strong(&globalValue, &val, val + 1)) {atomic_signal_fence(__ATOMIC_ACQ_REL);}
        else
            while (!atomic_compare_exchange_strong(&globalValue, &val, val - 1)) {atomic_signal_fence(__ATOMIC_ACQ_REL);}
    }
    ++completedCount;
    return 0;
}

int Test_Maximum_Contentions()
{
    int32_t threadCount = 32;
    thrd_t threads[threadCount];
    int64_t args[threadCount * 2];
    // Dispatch 8 Threads
    for (int32_t threadIndex = 0; threadIndex < threadCount; ++threadIndex)
    {
        args[threadIndex * 2] = (int64_t)(threadIndex % 2);
        args[threadIndex * 2 + 1] = (int64_t) 1048576;
        thrd_create(&threads[threadIndex], ApplyOperationThread, &args[threadIndex*2]);
    }

    while (completedCount != threadCount)
    {
    }
    int32_t val = globalValue;
    if (val != 0)
    {
        fprintf(stderr, "Incorrect value: %i\n", val);
        return 1;
    }
    for (int32_t i = 0; i < threadCount; ++i)
    {
        thrd_detach(threads[i]);
    }
    completedCount = 0;
    return 0;
}

_Atomic intptr_t* data;

void* DesequenceThread(void *arg)
{
    for (int64_t i = 0; i < 1048576; ++i)
    {
        _Atomic intptr_t* current = data;
        while (!atomic_compare_exchange_strong(data, current, current[1]))
        {
            atomic_thread_fence(__ATOMIC_ACQ_REL);
        }
       
    }
    atomic_fetch_add(&completedCount, 1);
    return NULL;
}

int Test_Sequence_Contentions()
{
    int32_t threadCount = 8;
    pthread_t* threads = (pthread_t*) calloc(threadCount, sizeof(pthread_t));
    data = (_Atomic intptr_t*)calloc(sizeof(_Atomic(intptr_t)), threadCount * 1048576 + 2);
    for (int i = 0; i < threadCount * 1048576 - 1; ++i)
    {
        data[i] = (intptr_t) &data[i + 1];
    }
    data[threadCount * 1048576 - 1] = (intptr_t) NULL;
    _Atomic intptr_t* start = data;
    for (int32_t threadIndex = 0; threadIndex < threadCount; ++threadIndex)
    {
        pthread_create(&threads[threadIndex], NULL, DesequenceThread, NULL);
    }

    while (completedCount != threadCount)
    {
    }

    completedCount = 0;
    free((void*)start);
    free(threads);
    return 0;
}

int main(int argc, char* argv[])
{
    if (argc > 0)
    {
        for (int i = 0; i < argc; ++i)
        {
            if (strcmp(argv[i], "test_maximum_contentions") == 0)
            {
                return Test_Maximum_Contentions();
            }
            if (strcmp(argv[i], "test_sequence_contentions") == 0)
            {
                return Test_Sequence_Contentions();
            }
        }
    }
    else
    {
        printf("Please specify which test to run.\n");
    }
    return 0;
}
