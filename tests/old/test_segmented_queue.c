#include "stdint.h"
#define SEGMENTED_QUEUE_TYPE int32_t
#include "src/segmented_queue_impl.c"

typedef struct {
    int a;
    uint32_t b;
    double c;
} TestStruct;

#define SEGMENTED_QUEUE_TYPE TestStruct
//#include "src/segmented_queue_impl.c"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int Test_PushItem()
{
    int status = 0;
    uint64_t outEnsegmented_queueCount = 0;
    uint64_t outCount = 0;
    segmented_queue_int32_t* segmented_queue = segmented_queue_int32_t_new();
    if (segmented_queue == NULL)
        status = 1;
    if (segmented_queue_int32_t_push(segmented_queue, 1, &outEnsegmented_queueCount))
        status = 2;
    if (!status && outEnsegmented_queueCount != 1)
        status = 3;
    if (!status && segmented_queue_int32_t_getcount(segmented_queue, &outCount))
        status = 4;
    if (!status && outCount != 1)
        status = 5;
    
    segmented_queue_int32_t_destroy(segmented_queue);
    return status;
}

int Test_PopItem()
{
    int status = 0;
    return status;
}

int main(int argc, char* argv[])
{
    if (argc > 0)
    {
        for (int i = 0; i < argc; ++i)
        {
            if (strcmp(argv[i], "test_pushitem") == 0)
            {
                return Test_PushItem();
            }
            if (strcmp(argv[i], "test_popitem") == 0)
            {
                return Test_PopItem();
            }
        }
    }
    else
    {
        printf("Please specify which test to run.\n");
    }
    return 0;
}