#include "stdint.h"
#define INDEX_TYPE int16_t
#define LIST_TYPE int32_t
#include "list_impl.c"

typedef struct {
    int a;
    uint32_t b;
    double c;
} TestStruct;

#define LIST_TYPE TestStruct
#include "list_impl.c"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int Test_AddItem()
{
    int status = 0;
    list_int32_t* list = list_int32_t_new();
    list_int32_t_add(list, 3);
    if (list->items[0] != 3)
        status = 1;

    list_int32_t_add(list, 2);
    if (!status && list->items[1] != 2)
        status = 1;

    list_int32_t_add(list, 1);
    if (!status && list->items[2] != 1)
        status = 1;
    if (!status && list->capacity < list->size)
        status = 1;
    list_int32_t_destroy(list);
    return status;
}

int Test_IndexOf1()
{
    int status = 0;
    INDEX_TYPE index = 0;
    list_int32_t* list = list_int32_t_new();
    list_int32_t_add(list, 1);
    list_int32_t_add(list, 2);
    list_int32_t_add(list, 3);
    if (!list_int32_t_indexof(list, 1, &index) && index != 0)
        status = 1;
    if (!status && (list_int32_t_indexof(list, 2, &index) || index != 1))
        status = 2;
    if (!status && (list_int32_t_indexof(list, 3, &index) || index != 2))
        status = 3;
    list_int32_t_destroy(list);
    return status;
}

int Test_IndexOf2()
{
    int status = 0;
    list_int32_t* list = list_int32_t_new();
    list_int32_t_add(list, 1);
    list_int32_t_add(list, 2);
    list_int32_t_add(list, 3);
    INDEX_TYPE index = 0;
    if (!list_int32_t_indexof2(list, 1, 1, &index))
        status = 1;
    if (!status && (list_int32_t_indexof2(list, 1, 0, &index) || index != 0))
        status = 2;
    if (!status && (list_int32_t_indexof2(list, 2, 1, &index) || index != 1))
        status = 3;
    if (!status && !list_int32_t_indexof2(list, 2, 2, &index))
        status = 4;
    if (!status && (list_int32_t_indexof2(list, 3, 2, &index) || index != 2))
        status = 5;
    if (!status && !list_int32_t_indexof2(list, 3, 3, &index))
        status = 6;
    list_int32_t_destroy(list);
    return status;
}

int Test_IndexOf3()
{
    int status = 0;
    list_int32_t* list = list_int32_t_new();
    list_int32_t_add(list, 1);
    list_int32_t_add(list, 2);
    list_int32_t_add(list, 3);
    INDEX_TYPE index = 0;
    if (!list_int32_t_indexof3(list, 1, 0, 0, &index))
        status = 1;
    if (!status && (list_int32_t_indexof3(list, 1, 0, 4, &index) || index != 0))
        status = 2;
    if (!status && (list_int32_t_indexof3(list, 1, 0, 1, &index) || index != 0))
        status = 3;
    if (!status && !list_int32_t_indexof3(list, 1, 1, 1, &index))
        status = 4;
    
    if (!status && (list_int32_t_indexof3(list, 2, 1, 1, &index) || index != 1))
        status = 5;
    if (!status && !list_int32_t_indexof3(list, 2, 1, 0, &index))
        status = 6;
    if (!status && !list_int32_t_indexof3(list, 2, 0, 1, &index))
        status = 7;
    if (!status && (list_int32_t_indexof3(list, 2, 0, 2, &index) || index != 1))
        status = 8;
    if (!status && !list_int32_t_indexof3(list, 2, 2, 2, &index))
        status = 9;

    if (!status && (list_int32_t_indexof3(list, 3, 2, 1, &index) || index != 2))
        status = 10;
    if (!status && !list_int32_t_indexof3(list, 3, 1, 1, &index))
        status = 11;
    if (!status && !list_int32_t_indexof3(list, 3, 2, 0, &index))
        status = 12;
    if (!status && (list_int32_t_indexof3(list, 3, 0, 3, &index) || index != 2))
        status = 13;
    if (!status && (list_int32_t_indexof3(list, 3, 1, 2, &index) || index != 2))
        status = 14;
    list_int32_t_destroy(list);
    return status;
}

int Test_Get()
{
    int status = 0;
    list_int32_t* list = list_int32_t_new();
    list_int32_t_add(list, 1);
    list_int32_t_add(list, 2);
    list_int32_t_add(list, 3);
    int result;
    if (list_int32_t_get(list, 0, &result)) status = 1;
    if (!status && result != 1) status = 2;
    result = -1;
    if (!status && list_int32_t_get(list, 1, &result)) status = 3;
    if (!status && result != 2) status = 4;
    result = -1;
    if (!status && list_int32_t_get(list, 2, &result)) status = 5;
    if (!status && result != 3) status = 6;
    list_int32_t_destroy(list);
    return status;
}

int Test_GetRange()
{
    int status = 0;
    list_int32_t* list = list_int32_t_new_withcapacity(1024);
    for (INDEX_TYPE i = 0; i < 1024; ++i)
        list_int32_t_add(list, i);
    int32_t* result = (int32_t*)calloc(sizeof(int32_t), 1024);
    if (result == NULL)
        status = 1;
    if (list_int32_t_getrange(list, 0, 1024, result))
        status = 2;
    if (!status && result == NULL)
        status = 3;
    if (!status && memcmp(result, list->items, list->size * sizeof(int32_t)) != 0)
        status = 4;
    if (!status && list_int32_t_getrange(list, 1, 1, result))
        status = 5;
    if (!status && result[0] != 1)
        status = 6;
    free(result);
    list_int32_t_destroy(list);
    return status;
}

int Test_AddRange()
{
    int status = 0;
    list_int32_t* list = list_int32_t_new();
    int* sample = (int*)malloc(1024 * sizeof(int));
    for (INDEX_TYPE i = 0; i < 1024; ++i)
        sample[i] = i + 1;
    if (list_int32_t_addrange(list, &sample[512], 512)) status = 1;
    if (!status && list->items[0] != 513) status = 2;
    if (!status && list->items[511] != 1024) status = 3;
    if (!status && list_int32_t_addrange(list, sample, 512)) status = 4;
    if (!status && list->items[512] != 1) status = 5;
    if (!status && list->items[1023] != 512) status = 6;
    list_int32_t_destroy(list);
    free(sample);
    return status;
}

int Test_AddRange_At()
{
    int status = 0;
    list_int32_t* list = list_int32_t_new();
    int32_t* sample = (int32_t*)calloc(1024, sizeof(int32_t));
    for (INDEX_TYPE i = 0; i < 1024; ++i)
        sample[i] = i;
    
    // 0 ... 512
    if (list_int32_t_addrange_at(list, 0, sample, 512)) status = 1;
    if (!status && list->items[0] != 0) status = 2;
    if (!status && list->items[511] != 511) status = 3;
    // 0 .. 255, 512 .. 1023, 256 .. 511
    if (!status && list_int32_t_addrange_at(list, 256, &sample[512], 512)) status = 4;
    if (!status && list->items[256] != 512) status = 5;
    if (!status && list->items[255] != 255) status = 6;
    if (!status && list->items[768] != 256) status = 7;
    if (!status && list->items[1023] != 511) status = 8;
    free(sample);
    list_int32_t_destroy(list);
    return status;
}

int Test_Remove()
{
    int status = 0;
    list_int32_t* list = list_int32_t_new();
    list_int32_t_add(list, 1);
    list_int32_t_add(list, 2);
    list_int32_t_add(list, 3);
    if (list_int32_t_remove(list, 0)) status = 1;
    if (!status && list->size != 2) status = 2;
    if (!status && list->items[0] != 2) status = 3;
    if (!status && list->items[1] != 3) status = 4;
    if (!status && list_int32_t_remove(list, 0)) status = 5;
    if (!status && list->items[0] != 3) status = 6;
    if (!status && list->size != 1) status = 7;
    if (!status && list_int32_t_remove(list, 0)) status = 8;
    if (!status && list->size != 0) status = 9;
    if (!status && list_int32_t_remove(list, 0)) status = 10;
    list_int32_t_destroy(list);
    return status;
}

int Test_RemoveRange()
{
    int status = 0;
    INDEX_TYPE outCount = 0;
    list_int32_t* list = list_int32_t_new();
    // 0 .. 1023
    for (int i = 0; i < 1024; ++i)
        list_int32_t_add(list, i);
    
    if (list_int32_t_count(list, &outCount)) status = 1;
    if (!status && outCount != 1024) status = 2;
    
    // 2 .. 1023
    if (!status && list_int32_t_removerange(list, 0, 2)) status = 3;
    if (!status && list->items[0] != 2) status = 4;
    if (!status && list->size != 1022) status = 5;

    // 2 .. 102, 104 .. 1023
    if (!status && list_int32_t_removerange(list, 100, 2)) status = 6;
    if (!status && list->items[99] != 101) status = 7;
    if (!status && list->items[100] != 104) status = 8;
    if (!status && list->items[101] != 105) status = 9;
    if (!status && list->size != 1020) status = 10;

    if (!status && list_int32_t_removerange(list, 1019, 100)) status = 11;
    if (!status && list->size != 1019) status = 12;

    list_int32_t_destroy(list);
    return status;
}

int Test_RemoveItem()
{
    int status = 0;
    list_int32_t* list = list_int32_t_new();
    list_int32_t_add(list, 1);
    list_int32_t_add(list, 2);
    list_int32_t_add(list, 3);

    if (list_int32_t_remove_item(list, 2)) status = 1;
    if (!status && list->size != 2) status = 2;
    if (!status && list->items[0] != 1) status = 3;
    if (!status && list->items[1] != 3) status = 4;

    if (!status && list_int32_t_remove_item(list, 1)) status = 5;
    if (!status && list->size != 1) status = 6;
    if (!status && list->items[0] != 3) status = 7;
    
    if (!status && list_int32_t_remove_item(list, 3)) status = 8;
    
    if (!status && list->size != 0) status = 9;
    if (!status && !list_int32_t_remove_item(list, 0)) status = 10;

    list_int32_t_destroy(list);
    return status;
}

int Test_StructList()
{
    int status = 0;
    list_TestStruct* list = list_TestStruct_new();
    TestStruct val = { .a = 1, .b = 2, .c = 3};
    if (list_TestStruct_add(list, val)) status = 1;
    val = (TestStruct) { .a = 2l, .b = 3u, .c = 1};
    if (!status && list_TestStruct_add(list, val)) status = 2;
    val = (TestStruct) { .a = 3l, .b = 2u, .c = 1};
    if (!status && list_TestStruct_add(list, val)) status = 3;
    INDEX_TYPE index = 0;
    if (!status && (list_TestStruct_indexof(list, val, &index) || index != 2)) status = 4;

    list_TestStruct_destroy(list);
    return status;
}

int Test_IncorrectData()
{
    int status = 0;
    list_int32_t* list = list_int32_t_new();
    int32_t output;
    if (!list_int32_t_get(list, 0, &output)) status = 1;
    list_int32_t_destroy(list);
    return status;
}

int Test_Reallocation()
{
    int status = 0;
    list_int32_t* list = list_int32_t_new();
    INDEX_TYPE outCount = 0;
    for (int i = 0; i < 1024; ++i)
    {
        if (list_int32_t_add(list, i)) status = 1;
    }
    if (!status && list_int32_t_count(list, &outCount)) status = 2;
    if (!status && outCount != 1024) status = 3;
    if (!status && list->size != 1024) status = 4;
    if (!status && list->capacity < list->size) status = 5;
    
    if (!status && list_int32_t_removerange(list, 254, 769)) status = 6;
    if (!status && list->size != 255) status = 7;
    if (!status && list->capacity < list->size*2) status = 8;
    if (!status && list->capacity > list->size*3) status = 9;
    list_int32_t_destroy(list);
    return status;
}

int main(int argc, char* argv[])
{
    if (argc > 0)
    {
        for (int i = 0; i < argc; ++i)
        {
            if (strcmp(argv[i], "test_additem") == 0)
            {
                return Test_AddItem();
            }
            if (strcmp(argv[i], "test_indexof1") == 0)
            {
                return Test_IndexOf1();
            }
            if (strcmp(argv[i], "test_indexof2") == 0)
            {
                return Test_IndexOf2();
            }
            if (strcmp(argv[i], "test_indexof3") == 0)
            {
                return Test_IndexOf3();
            }
            if (strcmp(argv[i], "test_get") == 0)
            {
                return Test_Get();
            }
            if (strcmp(argv[i], "test_getrange") == 0)
            {
                return Test_GetRange();
            }
            if (strcmp(argv[i], "test_addrange") == 0)
            {
                return Test_AddRange();
            }
            if (strcmp(argv[i], "test_addrange_at") == 0)
            {
                return Test_AddRange_At();
            }
            if (strcmp(argv[i], "test_remove") == 0)
            {
                return Test_Remove();
            }
            if (strcmp(argv[i], "test_removerange") == 0)
            {
                return Test_RemoveRange();
            }
            if (strcmp(argv[i], "test_removeitem") == 0)
            {
                return Test_RemoveItem();
            }
            if (strcmp(argv[i], "test_structlist") == 0)
            {
                return Test_StructList();
            }
            if (strcmp(argv[i], "test_incorrectdata") == 0)
            {
                return Test_IncorrectData();
            }
            if (strcmp(argv[i], "test_reallocation") == 0)
            {
                return Test_Reallocation();
            }
        }
    }
    else
    {
        printf("Please specify which test to run.\n");
    }
    return 0;
}
