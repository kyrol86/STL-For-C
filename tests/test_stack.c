#include <stdint.h>
#define STACK_TYPE int32_t
#include "src/stack_impl.c"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int Test_PushAndPop() {
  int status = 0;
  stack_int32_t *stack = stack_int32_t_new();
  int32_t popItem;
  uint64_t outStackCount = 0;
  uint64_t outCount = 0;

  // This section tests the followings:
  // 1. Ensure Push Item works
  // 2. Ensure Pop Item works
  // 3. Verify that ciruclar buffer works
  for (int32_t i = 0; i < 64; ++i) {
    outStackCount = 0;
    if (!status && stack_int32_t_push(stack, i, &outStackCount))
      status = 1;
    if (!status && outStackCount != 1)
      status = 2;
  }
  outCount = 0;
  if (!status && stack_int32_t_getcount(stack, &outCount))
    status = 3;
  if (!status && outCount != 64)
    status = 4;

  for (int32_t i = 63; i > -1; --i) {
    popItem = 0;
    if (!status && stack_int32_t_pop(stack, &popItem))
      status = 5;
    if (!status && popItem != i)
      status = 6;
  }
  outCount = 0;
  if (!status && stack_int32_t_getcount(stack, &outCount))
    status = 7;
  if (!status && outCount != 0)
    status = 8;

  stack_int32_t_destroy(stack);
  return status;
}

int Test_PushAndPopRange() {
  int status = 0;
  stack_int32_t *stack = stack_int32_t_new();
  if (stack == NULL)
    return 1;
  int32_t *rawData = (int32_t *)malloc(sizeof(int32_t) * 64);
  if (rawData == NULL)
    status = 2;
  if (!status)
    for (int32_t i = 0; i < 64; ++i)
      rawData[i] = i;
  if (!status && stack_int32_t_push_items(stack, rawData, 0, 64, NULL))
    status = 3;

  int32_t *popData = (int32_t *)calloc(sizeof(int32_t), 64);
  if (popData == NULL)
    status = 4;
  if (!status && stack_int32_t_pop_items(stack, 64, popData, NULL))
    status = 5;
  if (!status)
    for (int32_t i = 0; i < 64; ++i) {
      if (popData[i] != 63 - i)
        status = 6;
    }
  stack_int32_t_destroy(stack);
  return status;
}

int Test_CircularBuffer() {
  int status = 0;
  uint64_t pushCount = 0;
  uint64_t popCount = 0;
  stack_int32_t *stack = stack_int32_t_new();
  if (stack == NULL)
    return 1;
  int32_t *rawData = (int32_t *)malloc(sizeof(int32_t) * 1024);
  if (rawData == NULL)
    status = 2;
  int32_t *popData = (int32_t *)malloc(sizeof(int32_t) * 1024);
  if (popData == NULL)
    status = 3;

  // Simulate real-world use of stack
  for (uint64_t retry = 0; retry < 1000; ++retry) {
    int32_t length = ((int32_t)rand() % 1023) + 1;

    for (int32_t dataIndex = 0; dataIndex < length; dataIndex++)
      rawData[dataIndex] = (int32_t)rand();

    if (length == 1 || length % 2 == 0) {
        for (int32_t i = 0; i < length; ++i)
        {
            if (!status && stack_int32_t_push(stack, rawData[i], &pushCount))
                status = 4;
            if (!status && pushCount != 1)
                status = 5;
        }
    }
    else
    {
        if (!status && stack_int32_t_push_items(stack, rawData, 0, length, &pushCount))
            status = 6;
        if (pushCount != length)
            status = 7;
    }
    
    if (length == 1 || length % 2 == 1) {
        for (int32_t i = 0; i < length; ++i)
        {
            if (!status && stack_int32_t_pop(stack, popData))
                status = 8;
            if (!status && popData[0] != rawData[(length - 1) - i])
                status = 9;
        }
    }
    else
    {
        if (!status && stack_int32_t_pop_items(stack, length, popData, &popCount))
            status = 10;
        if (!status && popCount != length)
            status = 11;
        
        for (int32_t i = 0; i < length; ++i)
            if (!status && popData[i] != rawData[(length - 1) - i])
                status = 12;
    }
  }

  free(popData);
  free(rawData);
  stack_int32_t_destroy(stack);
  return status;
}

int main(int argc, char *argv[]) {
  if (argc > 0) {
    for (int i = 0; i < argc; ++i) {
      if (strcmp(argv[i], "test_pushandpop") == 0) {
        return Test_PushAndPop();
      }
      if (strcmp(argv[i], "test_pushandpoprange") == 0) {
        return Test_PushAndPopRange();
      }
      if (strcmp(argv[i], "test_circularbuffer") == 0) {
        return Test_CircularBuffer();
      }
    }
  } else {
    printf("Please specify which test to run.\n");
  }
  return 0;
}
