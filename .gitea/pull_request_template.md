#### Prerequisites 

- [ ] I have read the [contributing documentation](https://codeberg.org/CLanguagePurist/STL-For-C/wiki/How-to-contribute-to-STL-For-C-project)
- [ ] I have made sure to submit pull request to staging branch instead of main branch.
- [ ] My code follows the code style of this project.
- [ ] I have updated the documentation accordingly.
- [ ] I have added tests to cover my changes.
- [ ] All new and existing tests passed.

#### Description

    [Describe what your pull request intend to change.]
