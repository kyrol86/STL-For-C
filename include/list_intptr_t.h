#ifndef VOX_LIST_INTPTR_T_H
#define VOX_LIST_INTPTR_T_H 1
#include <stdint.h>
#define LIST_TYPE intptr_t
#include "include/list.h"
#undef LIST_TYPE
#endif