# Standard Template Library for C
[![Chat on Matrix](https://img.shields.io/matrix/stl_for_c:matrix.org)](https://matrix.to/#/#stl_for_c:matrix.org) ![License](https://img.shields.io/github/license/CLanguagePurist/STL-For-C)

A free library of generic data types for C programming language and any other programming languages.

### List of Available Types
1. List/Vector (Non-concurrent)
2. Queue (Non-concurrent)
3. Stack (Non-concurrent)
4. Dictionary/Hashtable (Non-concurrent)
5. Event Handler (Non-concurrent)

### Current To-Do List
1. Adding more test cases for various data containers for non-concurrent
2. Finish up documentations for currently implemented containers

### Work in Progress Types
1. Concurrent List/Vector
2. Concurrent Dictionary/Hashtable
3. Concurrent Queue (Pending rewrite)
4. Concurrent Stack (Pending rewrite)
9. Got an idea? Submit an idea in [discussion](https://codeberg.org/CLanguagePurist/STL-For-C/issues)!

### Future Features
1. Generic Search Features
2. Generic Sorting Features
3. Vectorization/SIMD Optimization

### Pending Evaluation
1. Hashset
2. Single Linked List
    Shelved for now in favor of supporting important data containers first
3. Doubly Linked List
    Shelved for now in favor of supporting important data containers first

## Why STL for C?
1. Usable Data Containers for C Programming Language with or without thread safety
2. Templated data types in a sense that this library leverages preprocessor to allow for data type substitution for each container type avoiding a pointer indirection performance cost that most data container in C are written for.
3. Foreign Function Interface considerations, other programming languages can use this too!
4. Refrains from reallocating needlessly, most of the data containers utilize circular buffers internally
5. "Why isn't something like this made already?"

## How to use this library?

[**Read the Wiki**](https://codeberg.org/CLanguagePurist/STL-For-C/wiki)

## Licensing

This library is licensed under MIT license which you can see [here for more information.](LICENSE)

## Reminder that we have a Warranty Disclaimer, use at your own risk!

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
